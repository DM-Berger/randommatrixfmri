% ftp://openpain.org/

close all
clear
addpath /Users/gmatharo/BCT/2017_01_15_BCT  %change if necessary
filebase1 = '/Users/gmatharo/NEURO_PROJECT/Hashmi/rest_visit_1/';
text1 = textread(['/Users/gmatharo/NEURO_PROJECT/Hashmi/rest_visit_1/rest_list'],'%s');
%textread(['roilist_new.txt'],'%s');

%%%%%%% filter for bold %%%%%%%%%%%%
Wn = [0.07];
[b,a] = butter(4, Wn/0.2, 'low'); % b, a to be used in filtfilt below
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ct1=1;

nSubj = length(text1); % number of subjects
thresh = 0.05:0.05:0.05; % this just evaluates to 0.05
nthresh = length(thresh); % this is just 1


for  i=1:nSubj
   
    for k=1:nthresh % this loop only runs once

        % Get all ROI data for subject i?
        % seems to load a 4D matrix
        b1_all = load([filebase1 text1{i}]); % [] the [str1 str2] is just string concatenation

        nRoi = length(b1_all); % assumes longest dimension is ROI? 244 probably

        % so probably the 36 is time?
        sb = size(b1_all); %     64    64    36   244
        lb = sb(1,1); % 64
        nb = sb(1,2); % 64

        % initialize for saving loop values
        b2_all = zeros(lb,nb);
        for nodes = 1:nb
            b1 = b1_all(:,nodes);
            b2 = 100*(b1 - mean(b1))/mean(b1); % normalize / scale
            % b, a defined at top of file
            bf = filtfilt(b,a,b2); % zero phase digital filtering
            b2_all(:,nodes) = bf;
        end
        %plot(b2_all);
        
        cc=corr(b2_all); % probably a 64 * 64 matrix
        [V,D]=eig(cc);
        lambda=double(sort(diag(D)));

        % save sorted eigenvalues in eval_test, to my_data.out, and
        % write this variable to a file in ASCII format
        eval_test(:,i)=lambda;
        save my_data.out eval_test -ASCII;
    %    type my_data.out;
        plot(lambda);


        % append sorted eigenvalues to evalues.dat, separated by newlines
        % e.g.
        % 1.123123
        % 0.312134
        % ...
        % NOTE: this outputs to `evalues.dat`, but no such file is read
        % in the FORTRAN code
        fileID = fopen('evalues.dat','w');
        fprintf(fileID,'%f\n',lambda);
        fclose(fileID);


%%%%%%%%%%%%%%%%% after here is probably irrelevant %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        for cols = 1:nRoi;
            asum=0.0;
                for rows =1:nRoi;
                    factor1 = V[rows,cols]*V[rows,cols];
                    factor=factor1*factor1;
                    asum=asum+factor;
                end
            aparp=nRoi*asum;
            parp=1.0/aparp;
        end

        parp_ratio(:,i)=parp;
        save parp_ratio.out parp_ratio -ASCII;
        %imagesc(cc)   
        ccall(i,k,:,:)=cc;   
        fprintf('%f\n',thresh(k));
        
        cc_thr=threshold_proportional(cc,thresh(k));
        %imagesc(cc_thr)   
        
    
        
        %%hubs
        % # of shortest pat from i to j that must pass through the centrality.
        bet_b(i,k,:)=betweenness_bin(double(cc_thr>0));
        %G=digraph(i,k);
        %plot(G);
        % If a node connect to other nodes, It have high eigenvector centrality.
        eig_b(i,k,:)=eigenvector_centrality_und(double(cc_thr>0));
        
        % # of links connected to the node.
        deg_b(i,k,:)=degrees_und(double(cc_thr>0));
        % a positive assortativity indicates that nodes tend to link to other
        % nodes with the same or similar degree.
        assor_b(i,k)=assortativity_bin(double(cc_thr>0),0);
                
        
        
        
        %%clustering
        % a weighted sum of closed walks of different lengths in the network starting and ending at the node.
        subG_b(i,k,:)=subgraph_centrality(double(cc_thr>0));
        % similar to betweenness centrality, but computes centrality based on on local neighborhoods.
        flowC_b(i,k,:)=flow_coef_bd(double(cc_thr>0));
        % the fraction of # of triangles around a node and # of connected triples
        clus_b(i,k,:)=clustering_coef_bu(double(cc_thr>0));
        % The local efficiency is ratio of # of connections between i's
        % neighbors to total # possible
        effL_b(i,k,:) =  efficiency_bin(double(cc_thr>0),1);
                
        
    
        %%paths
        % whether pairs of nodes are connected by paths
        temp=reachdist(double(cc_thr>0));
        reach_b(i,k)=sum(temp(:,1)<1);
        % lengths of shortest paths between all pairs of nodes
        % the average shortest path length in the network
        charpath_b(i,k)=charpath(distance_bin(double(cc_thr>0)));
        effG_b(i,k)=efficiency_bin(double(cc_thr>0));
        
        
        % modularity
        [M(i,k,:),Q(i,k)] = community_louvain(cc_thr>0);
        % path_transitivity
        [T1]=path_transitivity(double(cc_thr>0),[]);
        T_bin(i,k,:,:)=T1;
        T_bin_mean=mean(mean(T_bin));
        T_bin_squeeze=squeeze(mean(T_bin));
        
        
        %[T2]=path_transitivity(cc_thr,'inv');
        %T_wei_inv(i,k,:,:)=T2;
        %T_wei_inv_mean=mean(mean(T_wei_inv));


        [T3]=path_transitivity(cc_thr,'log');
        T_wei_log(i,k,:,:)=T3;
        T_wei_log_mean=mean(mean(T_wei_log));
        T_wei_log_squeeze=squeeze(mean(T_wei_log));
    end
      
end
