close all
clear
addpath /Users/gmatharo/BCT/2017_01_15_BCT  %change if necessary
filebase1 = '/Users/gmatharo/NEURO_PROJECT/Hashmi/rest_visit_1/';
text1 = textread(['/Users/gmatharo/NEURO_PROJECT/Hashmi/rest_visit_1/rest_list'],'%s');
%textread(['roilist_new.txt'],'%s');
%%%%%%%filter for bold%%%%%%%%%%%%
Wn =[0.07];
[b,a] = butter(4,Wn/0.2,'low');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ct1=1;
ns = length(text1); % number of subjects
nSubj=ns;
nSubj
thresh=0.05:0.05:0.05;
nthresh=length(thresh);
nthresh

for  i=1:nsubj;
   
    for k=1:nthresh
    i
    k
    b1_all = load([filebase1 text1{i}]);
    nRoi=length(b1_all);
    sb = size(b1_all);
    lb = sb(1,1);
    nb = sb(1,2);
    b2_all = zeros(lb,nb);
        for nodes = 1:nb;
            b1 = b1_all(:,nodes);
            b2 = 100*(b1 - mean(b1))/mean(b1);
            bf = filtfilt(b,a,b2);
            b2_all(:,nodes)=bf;
        end
        %plot(b2_all);
       
    cc=corr(b2_all);
    [V,D]=eig(cc);
    lambda=double(sort(diag(D)));
    eval_test(:,i)=lambda;
    save my_data.out eval_test -ASCII;
%    type my_data.out;
    plot(lambda);
    fileID = fopen('evalues.dat','w');
     fprintf(fileID,'%f\n',lambda);
     fclose(fileID);
     
     for cols = 1:nRoi;
        asum=0.0;
         for rows =1:nRoi;
             factor1 = V[rows,cols]*V[rows,cols];
             factor=factor1*factor1;
             asum=asum+factor;
         end
        aparp=nRoi*asum;
        parp=1.0/aparp;
     end
     parp_ratio(:,i)=parp;
     save parp_ratio.out parp_ratio -ASCII;
    %imagesc(cc)   
    ccall(i,k,:,:)=cc;   
    fprintf('%f\n',thresh(k));
    
    cc_thr=threshold_proportional(cc,thresh(k));
    %imagesc(cc_thr)   
    
   
    
    %%hubs
    % # of shortest pat from i to j that must pass through the centrality.
    bet_b(i,k,:)=betweenness_bin(double(cc_thr>0));
    %G=digraph(i,k);
    %plot(G);
    % If a node connect to other nodes, It have high eigenvector centrality.
    eig_b(i,k,:)=eigenvector_centrality_und(double(cc_thr>0));
    
    % # of links connected to the node.
    deg_b(i,k,:)=degrees_und(double(cc_thr>0));
    % a positive assortativity indicates that nodes tend to link to other
    % nodes with the same or similar degree.
    assor_b(i,k)=assortativity_bin(double(cc_thr>0),0);
             
    
    
    
    %%clustering
    % a weighted sum of closed walks of different lengths in the network starting and ending at the node.
    subG_b(i,k,:)=subgraph_centrality(double(cc_thr>0));
    % similar to betweenness centrality, but computes centrality based on on local neighborhoods.
    flowC_b(i,k,:)=flow_coef_bd(double(cc_thr>0));
    % the fraction of # of triangles around a node and # of connected triples
    clus_b(i,k,:)=clustering_coef_bu(double(cc_thr>0));
    % The local efficiency is ratio of # of connections between i's
    % neighbors to total # possible
    effL_b(i,k,:) =  efficiency_bin(double(cc_thr>0),1);
            
    
   
    %%paths
    % whether pairs of nodes are connected by paths
    temp=reachdist(double(cc_thr>0));
    reach_b(i,k)=sum(temp(:,1)<1);
    % lengths of shortest paths between all pairs of nodes
    % the average shortest path length in the network
    charpath_b(i,k)=charpath(distance_bin(double(cc_thr>0)));
    effG_b(i,k)=efficiency_bin(double(cc_thr>0));
    
    
    % modularity
    [M(i,k,:),Q(i,k)] = community_louvain(cc_thr>0);
    % path_transitivity
    [T1]=path_transitivity(double(cc_thr>0),[]);
    T_bin(i,k,:,:)=T1;
    T_bin_mean=mean(mean(T_bin));
    T_bin_squeeze=squeeze(mean(T_bin));
    
    
    %[T2]=path_transitivity(cc_thr,'inv');
    %T_wei_inv(i,k,:,:)=T2;
    %T_wei_inv_mean=mean(mean(T_wei_inv));


    [T3]=path_transitivity(cc_thr,'log');
    T_wei_log(i,k,:,:)=T3;
    T_wei_log_mean=mean(mean(T_wei_log));
    T_wei_log_squeeze=squeeze(mean(T_wei_log));
    end
      
end
