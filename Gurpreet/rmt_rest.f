	implicit real*8(a-h,o-z)
        parameter(nsubj=18)! number of subjects, change if needed
        parameter(nval=69) ! 69 for rest, 116 for sp1, "test with 116 for rest"
        parameter(nsize=73) ! 69 for rest, 116 for sp1
        parameter(nmax=19) ! for binning
        parameter(nstt=110)
        dimension nbin(0:nmax),anav(0:nmax),xx(0:nmax)
        dimension nstat(nmax),ncstat(nmax),eaxis(nmax)
        dimension nsq(nstt),nsm(nstt),nst(nstt)
        dimension ns3(nstt),ns4(nstt),rgap(nstt)
        dimension wret(nval),fret(131,131)
        dimension avspac(nsubj), space_average(nsubj)
        dimension enorm(nval),ex(nsize+1),ey(nsize+1)
        dimension resid(nsize+1),space(nval-1)
        common /param/ wret
        common /param1/ power
        pi=4.0*datan(1.0d0)
        pisq=pi*pi
        open(1,file='rest_visit_4_REC.out',status='unknown') ! this is the input
        open(2,file='ncstat_sbp_RV4R.dat',status='unknown') ! this is output, nearest corr.
        open(3,file='var_gam_sbp_RV4R.dat',status='unknown') ! this is output
        open(4,file='spectra.dat',status='unknown') ! part of output
        open(5,file='ncstat_hist_RV4R.dat',status='unknown') ! part of output
        open(8,file='Sigma_theory.dat',status='unknown') ! output, long-ranged corr.
        
        power=3.35
        xmin=0.0d0
        xmax=3.0d0
        h=(xmax-xmin)/dfloat(nmax)
        do i=0,nmax
         nbin(i)=0
         anav(i)=0
        enddo

         do lh=1,nmax
           eaxis(lh)=(dfloat(lh)-0.5d0)*(3.0d0/nmax)
         enddo
         do jlk=1,nmax
          ncstat(jlk)=0
         enddo
         do kl=1,nstt
          nsq(kl)=0
          nsm(kl)=0
          nst(kl)=0
          ns3(kl)=0
          ns4(kl)=0
         enddo

        read(1,*)((fret(j,i),i=1,nsubj),j=1,131)

        smallest=100.0
        biggest=0.0
        do k=1,nsubj
         do j=1,nval
          wret(j)=0.0d0
         enddo
         do i = 59,127                        ! for resting
c         do i = 12,127                               ! for sp1
c         print*,abs(59-i)+1
          wret(abs(59-i)+1)=fret(i,k)               ! for resting
c          wret(abs(12-i)+1)=fret(i,k)                ! for sp1
          write(4,*)wret(abs(59-i)+1),abs(59-i)+1   ! for resting
c          write(4,*)wret(abs(12-i)+1),abs(12-i)+1    ! for sp1
         enddo

c now do curve fitting here
        call crvfit(a,b,c)
        print*,'a,b,c are',a,b,c
        svar=0.0d0
        nstart=1
        nlast=nval
        do j=nstart,nlast
c         yym=a-b*dexp(-c*(wret(j)**(1.0d0/5.0d0)))
c         print*,'yym is',yym
         ymis=dabs(dfloat(j)
     $-a+b*dexp(-c*(wret(j)**(1.0d0/power))))
c         write(25,*)j,ymis
        enddo

c now obtain "unfolded" eigenvalues, average spacings is unity.
         
        do ndt=1,nval
         x=wret(ndt)
          gx1=a-b*dexp(-c*(x**(1.0/power)))   ! change 5 to 3.35 for sp1
          write(25,*)gx1
         enorm(ndt)=gx1  ! these are unfolded eigenvalues
        enddo
        do l=1,nmax
         nstat(l)=0
        enddo
        sgap=3.0d0/float(nmax)
        do i=1,nval-1
         space(i)=enorm(i+1)-enorm(i)
c         write(26,*)space(i)
         ind=space(i)/sgap
         ic=ind+1
         if((ic.le.nmax).and.(ic.ge.1))nstat(ic)=nstat(ic)+1
        enddo
        do lp=1,nmax
         ncstat(lp)=ncstat(lp)+nstat(lp)
        enddo

c        smallest=100.0
c        biggest=0.000
        aver2=0.0d0
        do i=1,nval-1
          aver2=aver2+space(i)
        enddo
        aver2=aver2/dfloat(nval-1)
        space_average(k)=aver2
c        avspac(k)=aver2
        print*,'avspac is',aver2
        baver=0.0
        do i=1,nval-1
         baver=baver+((aver2-space(i))**2)
        enddo
        baver=baver/float(nval-1)
        avspac(k)=baver
        if(baver.lt.smallest) smallest=baver
        if(baver.gt.biggest) biggest=baver
        print*,'baver is',baver
        print*,'k over',k
       enddo ! end of k loop

c  calculate average spacing variance now

       aver=0.0
       bver=0.0
       do i=1,nsubj
        write(10,*)i,avspac(i)
        aver=aver+avspac(i)
        bver=bver+space_average(i)
       enddo
       aver=aver/float(nsubj)
       bver=bver/float(nsubj)
       print*,'average spacing is',sngl(bver)
       print*,'average spacing variance is',sngl(aver),sngl(smallest),
     $sngl(biggest)
c       baver=0.0
c       do i=1,nsubj
c        baver=baver+((aver-avspac(i))**2)
c       enddo
c       baver=baver/float(nsubj)
c       print*,'baver is',baver

c   calculate nearest neighbor spacings distribution, make area 1 with xmgrace
        asum=0.0d0
        do lu=1,nmax
         asum=asum+ncstat(lu)
        enddo
         write(2,*)0.00000, 0.0000
        do lu=1,nmax
         write(2,*)eaxis(lu),float(ncstat(lu))/(k*sgap*nval)
         write(5,*)eaxis(lu),'0.0'
         write(5,*)eaxis(lu),float(ncstat(lu))/(k*sgap*nval)
         write(5,*)eaxis(lu)+sgap,float(ncstat(lu))/(k*sgap*nval)
        enddo

c  calculation of sigma-square starts here
        ebegin=0.001d0+enorm(1)
        eend=enorm(nval)-0.001d0
        do lind=1,nstt
         rgap(lind)=0.05d0*lind
         stick=rgap(lind)
         elow=ebegin
4000     ehigh=elow+stick
         if(ehigh.gt.eend)go to 5732
c count how many levels are there between elow and ehigh
         ncount=0
         do lj=1,nval
          vc=enorm(lj) 
          if((vc.ge.elow).and.(vc.le.ehigh))ncount=ncount+1
          if(vc.gt.ehigh)go to 7854
         enddo
7854     continue
c  current value of ncount is the number of levels of interest
         ns3(lind)=ns3(lind)+ncount*ncount*ncount
         ns4(lind)=ns4(lind)+ncount*ncount*ncount*ncount
         nsq(lind)=nsq(lind)+ncount*ncount
         nsm(lind)=nsm(lind)+ncount
         nst(lind)=nst(lind)+1
c         shift=(dfloat(2)/dfloat(11))+(dfloat(1)/dfloat(11))*stick
         shift=stick/100.0d0
         elow=elow+shift
         go to 4000
5732     continue
        enddo
        do lb=1,nstt
          av=dfloat(nsm(lb))/dfloat(nst(lb))
          avsq=dfloat(nsq(lb))/dfloat(nst(lb))
          avs3=dfloat(ns3(lb))/dfloat(nst(lb))
          avs4=dfloat(ns4(lb))/dfloat(nst(lb))
          var=avsq-av*av
          sig3=avs3-3*avsq*av+3*av*av*av-av*av*av
        sig4=avs4-4*avs3*av+6*avsq*av*av+av*av*av*av-4*av*av*av*av
          beta1=sig3*sig3/(var*var*var)
          beta2=sig4/(var*var)
          gamma1=dsqrt(beta1)
          gamma2=beta2-3.0d0
          sig_th=(2.0/pisq)*(log(2.0*pi*rgap(lb))+1.5772-(pisq/8.0))
          write(3,*)sngl(rgap(lb)),sngl(var),sngl(gamma1),sngl(gamma2)
          write(8,*)sngl(rgap(lb)),sngl(sig_th),sngl(rgap(lb))
        enddo
        
        stop 
	end
 
        subroutine crvfit(af,bf,cf)
        implicit real*8(a-h,o-z)
        cleft=1.95d0
        cright=2.00d0
        do jit=1,100
          cmidl=(cleft+cright)/2.0d0
          call cvalue(cleft,aleft,bleft,cvleft)
c       print*,'crvfit iter ',jit
c        print*,cleft,aleft,bleft,cvleft
          call cvalue(cright,aright,bright,cvright)
c       print*,cright,aright,bright,cvright
          call cvalue(cmidl,amidl,bmidl,cvmidl)
c       print*,cmidl,amidl,bmidl,cvmidl
          if((cvleft*cvmidl).lt.0.0d0)then
              cright=cmidl
                                      else
              cleft=cmidl
          endif
        enddo
        af=amidl
        bf=bmidl
        cf=cmidl
        return
        end

        subroutine cvalue(cfit,afit,bfit,cval)
        implicit real*8(a-h,o-z)
        parameter(ndf=69)  ! 69 for rest, 116 for sp1
        dimension wret(ndf)
        common /param/ wret
        common /param1/ power
        smy=0.0d0
        sme2cx=0.0d0
        smyec=0.0d0
        smec=0.0d0
        smyxec=0.0d0
        smxec=0.0d0
        smxe2c=0.0d0
        nstart=1
c       nlast=0.9d0*(ndf-6)
        nlast=(ndf)
        ndata=nlast-nstart+1
c        print*,'ndata is',ndata
c        do i=1,(ndf-6)
         do i=nstart,nlast
         xi=wret(i)
         yi=dfloat(i)
         xpi=dexp(-cfit*(xi**(1.0/power))) ! change 5 to 3.35 for sp1
         xp2i=xpi*xpi
         smy=smy+yi
         sme2cx=sme2cx+xp2i
         smyec=smyec+yi*xpi
         smec=smec+xpi
         smyxec=smyxec+yi*xi*xpi
         smxec=smxec+xi*xpi
         smxe2c=smxe2c+xi*xp2i
        enddo
c       den=(ndf-6)*sme2cx-smec*smec
        den=ndata*sme2cx-smec*smec
        afit=(smy*sme2cx-smyec*smec)/den
c       bfit=(smy*smec-(ndf-6)*smyec)/den
        bfit=(smy*smec-ndata*smyec)/den
        cval=smyxec-(afit*smxec-bfit*smxe2c)
        return
        end







