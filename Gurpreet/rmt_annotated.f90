! Implicit none says that you have to explicitly declare all your
! variables before using them. If you come up with a new variable in the
! middle of your source code, the compiler will complain and make you
! go back and declare it. You can declare variable whatever type and
! size you want to, but you have to do it yourself.

! Implicit REAL*8(A-H,O-Z), on the other hand, allows you to go the
! implicit route where you can come up with variables along the way, as
! needed, without having to declare first. Additionally, all those
! variables would be REAL*8, which is NOT the default size; default is
! REAL*4, or simply REAL, and this statement means that all variables
! with names beginning with either letter of 'a' to 'h' and 'o' to 'z'
! are declared real*8.
IMPLICIT REAL*8 (a - h, o - z)



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                         Declare Constants                          !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! The PARAMETER statement assigns a symbolic name to a constant.
! https://docs.oracle.com/cd/E19957-01/805-4939/6j4m0vn79/index.html
! i.e. these are constexpr consts, not variables, just aliases to values
PARAMETER ( NSUBJ = 18 )  ! number of subjects, change if needed
! NVAL == number of eigenvalues (per subject?)
PARAMETER ( NVAL  = 69 )  ! 69 for rest, 116 for sp1, "test with 116 for rest"
PARAMETER ( NSIZE = 73 )  ! 69 for rest, 116 for sp1

! below is commented as for "binning", but it is not clear where in the
PARAMETER ( NMAX  = 19 )  ! for binning
PARAMETER ( NSTT  = 110 )



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!              Declare a bunch of arrays to save values              !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! DIMENSION === array declaration, DIMENSION(`rank`, `shape`, and `extent`)
!       rank  == dimension (e.g. n x m x p array has rank 3)
!       shape ==
!       DIMENSION VARNAME(x, y, z) declares a 3-dimensional array
!       VARNAME where the allowed range of indices in each dimension
!       are [1,x], [1,y], [1,z]
! i.e. DIMENSION nbin(0:NMAX) declares than nbin is a 1-dimensional
! array allowing indexes from 0 to NMAX (all inclusive, i.e. [0, NMAX])
! and DIMENSION nstat(NMAX) declares the 1-D array nstat allowing indices
! in [1, NMAX]

! DIMENSION nbin(0:NMAX) ! unused
! DIMENSION anav(0:NMAX) ! unused
! DIMENSION xx(0:NMAX)

DIMENSION nstat(NMAX)
DIMENSION ncstat(NMAX)
DIMENSION eaxis(NMAX)

! these have something to do with nearest neighbor spacing dist
DIMENSION nsq(NSTT)
DIMENSION nsm(NSTT)
DIMENSION nst(NSTT)
DIMENSION ns3(NSTT)
DIMENSION ns4(NSTT)
DIMENSION rgap(NSTT)

! what is 131? a "large enough" size?
! this is a two-dimensional array 131 x 131
! it gets populated / initialized by a read call later, but this
! read call may in fact be reading less than 131 x 131 pieces of data
! from the file, since it seems to attempt to read 131 columns of data
! and NSUBJ rows
DIMENSION fret(131, 131)    ! what is 131?

DIMENSION wret(NVAL)        ! hold eigenvalues read from fret
DIMENSION enorm(NVAL)       ! save normalized eigenvalues
DIMENSION space(NVAL - 1)   ! save eigenvalue spacings

DIMENSION avspac(NSUBJ)
DIMENSION space_average(NSUBJ)

DIMENSION ex(NSIZE + 1)
DIMENSION ey(NSIZE + 1)
DIMENSION resid(NSIZE + 1)



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!           Declare Globals for Sharing Across Subroutines           !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! `COMMON /namespace/ name` basically declares a global variable `name`
! under the namespace `/namespace/`
! the 'ret' in wret appears to mean 'rest'
! appears to be 1D array with length (127-59), i.e. subjects 59 to 127
COMMON /param/ wret
COMMON /param1/ power ! initialized below to 3.35



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!         Initialize Convenience Variables for Main Routine          !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

pi = 4.0 * datan(1.0d0)     ! pi, not used until sigma^2 calculations
pisq = pi * pi              ! pi^2, not used until sigma^2 calculations
power = 3.35                ! used in e^(-c*x^(1/power))

! unused, except in `h` (in zero initializations), which is also unused
! xmin = 0.0d0
! xmax = 3.0d0



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                       Open Files for Writing                       !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! https://docs.oracle.com/cd/E19957-01/805-4939/6j4m0vnaf/index.html
! status - 'unknown' seems to mean create if not exist, else append to end

! presumably the 'graph_binary.m' output file gets renamed each time
! to something like 'rest_vist_4_REC.out' or something
open(1, file = 'rest_visit_4_REC.out', status = 'unknown') ! this is the input
open(2, file = 'ncstat_sbp_RV4R.dat', status = 'unknown') ! this is output,  nearest corr.
open(3, file = 'var_gam_sbp_RV4R.dat', status = 'unknown') ! this is output
open(4, file = 'spectra.dat', status = 'unknown') ! part of output
open(5, file = 'ncstat_hist_RV4R.dat', status = 'unknown') ! part of output
open(8, file = 'Sigma_theory.dat', status = 'unknown') ! output,  long-ranged corr.



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                 Whole Bunch of Zero-Initialiations                 !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! line unused below
! h = (xmax - xmin) / dble(NMAX) ! dble() just converts to double

! loops are inclusive of the range (i.e. do i = 0, 3 goes 0, 1, 2, 3)
! do i = 0, NMAX
!     nbin(i) = 0
!     anav(i) = 0 ! unused
! enddo

! create some kind of grid with NMAX  == 19 bins
do lh = 1, NMAX
    eaxis(lh) = (dble(lh) - 0.5d0) * (3.0d0 / NMAX)
enddo

do jlk = 1, NMAX ! == 19, again, seems very small somehow
    ncstat(jlk) = 0
enddo

do kl = 1, NSTT
    nsq(kl) = 0
    nsm(kl) = 0
    nst(kl) = 0
    ns3(kl) = 0
    ns4(kl) = 0
enddo



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                 ???                                !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Fortran arrays are stored in column-major order: A(3,2)
! A(1,1) A(2,1) A(3,1) A(1,2) A(2,2) A(3,2) A(1,3) A(2,3) A(3,3)
! i.e. if we have A = 2 x 2, A(1,2) == a_12
! https://en.wikipedia.org/wiki/Row-_and_column-major_order

! read from file descriptor 1, format * (list-directed IO) into `fret`
! i.e. read whatever 'rest_visit_4_REC.out' is

! in below, the `(fret(j, i), i = 1, NSUBJ)` as an 'implied DO list'
! the `i = 1, NSUBJ` expression defines `i` as the implied DO variable
! with initial variable 1, and limit (final, inclusive) value NSUBJ
! this means the line below is a 'nested implied do loop': see last
! example of https://pages.mtu.edu/~shene/COURSES/cs201/NOTES/chap08/io.html
! i.e. the below evaluates as:
! fret(1, 1), fret(1, 2), ... fret(1, NSUBJ), fret(2, 1), ...
! i.e. it reads the first data value to fret(1, 1), second to fret(1,2)
! so that each column of fret is a subject, each row is an eigenval
! i.e. column i contains subject i's sorted eigenvalues
! i.e. fret(:,i) has the eigenvalues for a subject
read(1, *)( (fret(j, i), i = 1, NSUBJ), j = 1, 131 )

smallest = 100.0
biggest = 0.0

do k = 1, NSUBJ ! for each subject / patient / participant k
    ! for each subjects eigenvalues, zero initialize wret
    do j = 1, NVAL
        wret(j) = 0.0d0
    enddo

    ! save values into wret, appears to be a "resting" subset of sorts
    ! for some reason only use some eigenvalues???
    ! are some eigenvalues from the resting state, some task?
    ! NOTE: 127 - 59 + 1 = 69!!!, i.e. NVAL
    do i = 59, 127 ! for resting
        !! load certain values from fret (file1) into wret
        !! load only eigenvalues [59, 127] into wret for some reason
        wret(abs(59 - i) + 1) = fret(i, k)       ! for resting

        ! write to file descriptor 4, 'spectra.dat', seems to be writing
        ! the eigenvalues, and then indices?
        write(4, *)wret(abs(59 - i) + 1), abs(59 - i) + 1   ! for resting
    enddo

    ! c now do curve fitting here
    ! call below seems to implicitly initialize a, b, c
    print*, 'a, b, c are', a, b, c

    ! call below assigns values to a, b, c
    call crvfit(a, b, c)

    svar = 0.0d0
    nstart = 1
    nlast = NVAL ! 69 in this code version

    ! literally does nothing, since ymis is unused
    ! do j = nstart, nlast
    !     ymis = abs(dble(j) - a + b * exp(-c * (wret(j)**(1.0d0 / power) ) )) ! dabs === double abs()
    ! enddo

    ! c now obtain "unfolded" eigenvalues,  average spacings is unity.
    do ndt = 1, NVAL ! NVAL == 69
        x = wret(ndt) ! this confirms wret holds eigenvalues
        gx1 = a - b * dexp(-c * (x**(1.0 / power)))   ! change 5 to 3.35 for sp1
        ! write(25, *)gx1 ! no idea what is being written to here, dangerous

        ! enorm == eigen normalized, but not yet scaled
        enorm(ndt) = gx1  ! these are unfolded eigenvalues
    enddo

    do l = 1, NMAX
        nstat(l) = 0
    enddo

    !! I don't understand why this block is doing what it is doing
    sgap = 3.0d0 / float(NMAX) ! will be used to scale, but why?
    do i = 1, NVAL - 1 ! remember, NVAL is number of eigenvalues
        ! calculate spacing between adjacent eigens
        space(i) = enorm(i + 1) - enorm(i)
        ! write(26, *)space(i) ! more dangerous writes to nothing
        ind = space(i) / sgap
        ic = ind+1
        if ((ic.le.NMAX).and.(ic.ge.1)) nstat(ic) = nstat(ic) + 1
    enddo

    ! this seems to be that step function thing maybe?
    do lp = 1, NMAX
        ncstat(lp) = ncstat(lp)+nstat(lp)
    enddo

    ! csmallest = 100.0 ! unused
    ! cbiggest = 0.000 ! unused

    ! sum all spacings
    aver2 = 0.0d0
    do i = 1, NVAL - 1
        aver2 = aver2 + space(i)
    enddo

    ! this is the rescaling to make eigenvalue density integrate to 1?
    aver2 = aver2 / dble(NVAL - 1)
    space_average(k) = aver2
    ! c avspac(k) = aver2
    print*, 'avspac is', aver2

    baver = 0.0
    do i = 1, NVAL - 1
        baver = baver + ((aver2 - space(i))**2)
    enddo
    baver = baver / float(NVAL - 1)
    avspac(k) = baver

    if(baver.lt.smallest) smallest = baver
    if(baver.gt.biggest) biggest = baver
    print*, 'baver is', baver
    print*, 'k over', k
enddo ! end of k loop

!  calculate average spacing variance now

aver = 0.0
bver = 0.0
do i = 1, NSUBJ
    write(10, *)i, avspac(i)
    aver = aver + avspac(i)
    bver = bver + space_average(i)
enddo

aver = aver / float(NSUBJ)
bver = bver / float(NSUBJ)
print*, 'average spacing is', sngl(bver)
print*, 'average spacing variance is', sngl(aver), sngl(smallest),  sngl(biggest)
!       baver = 0.0
!       do i = 1, NSUBJ
cbaver = baver + ((aver - avspac(i))**2)
!       enddo
!       baver = baver/float(NSUBJ)
!       print*, 'baver is', baver

!   calculate nearest neighbor spacings distribution,  make area 1 with xmgrace
asum = 0.0d0

do lu = 1, NMAX
    asum = asum + ncstat(lu)
enddo

write(2, *)0.00000,  0.0000

do lu = 1, NMAX
    write(2, *)eaxis(lu), float(ncstat(lu))/(k * sgap * NVAL)
    write(5, *)eaxis(lu), '0.0'
    write(5, *)eaxis(lu), float(ncstat(lu))/(k * sgap * NVAL)
    write(5, *)eaxis(lu)+sgap, float(ncstat(lu))/(k * sgap * NVAL)
enddo

!  calculation of sigma-square starts here
ebegin = 0.001d0+enorm(1)
eend = enorm(NVAL)-0.001d0
do lind = 1, NSTT
    rgap(lind) = 0.05d0*lind
    stick = rgap(lind)
    elow = ebegin
    4000     ehigh = elow+stick
    if(ehigh.gt.eend)go to 5732
    ! count how many levels are there between elow and ehigh
    ncount = 0

    do lj = 1, NVAL
        vc = enorm(lj)
        if((vc.ge.elow).and.(vc.le.ehigh))ncount = ncount+1
        if(vc.gt.ehigh)go to 7854
    enddo
    7854     continue
    !  current value of ncount is the number of levels of interest
    ns3(lind) = ns3(lind) + ncount * ncount * ncount
    ns4(lind) = ns4(lind) + ncount * ncount * ncount * ncount
    nsq(lind) = nsq(lind) + ncount * ncount
    nsm(lind) = nsm(lind) + ncount
    nst(lind) = nst(lind) + 1
    ! shift = (dble(2)/dble(11))+(dble(1)/dble(11))*stick
    shift = stick/100.0d0
    elow = elow+shift
    go to 4000
    5732     continue
enddo

do lb = 1, NSTT
    av = dble(nsm(lb))/dble(nst(lb))
    avsq = dble(nsq(lb))/dble(nst(lb))
    avs3 = dble(ns3(lb))/dble(nst(lb))
    avs4 = dble(ns4(lb))/dble(nst(lb))
    var = avsq - av * av
    sig3 = avs3 - 3 * avsq * av + 3 * av * av * av - av * av * av
    sig4 = avs4 - 4 * avs3 * av + 6 * avsq * av * av + av * av * av * av - 4 * av * av * av * av
    beta1 = sig3 * sig3/(var * var * var)
    beta2 = sig4 / (var * var)
    gamma1 = dsqrt(beta1)
    gamma2 = beta2 - 3.0d0
    sig_th = (2.0 / pisq) * (log(2.0 * pi * rgap(lb)) + 1.5772 - (pisq / 8.0))
    write(3, *)sngl(rgap(lb)), sngl(var), sngl(gamma1), sngl(gamma2)
    write(8, *)sngl(rgap(lb)), sngl(sig_th), sngl(rgap(lb))
enddo

stop
    end


! called with a, b, c in code, assigns fitted values af, bf, cf to those
! a, b, c variables passed in
!
! seems to be some kind of iterative fitting procedure, fitting an
! exponential of some kind via least squares, with linear equations
! of the exponentiated `x` values from the patients
!
! also appears to behave like binary search, squeezing together to some
! value
subroutine crvfit(af, bf, cf)
    implicit real*8(a-h, o-z)
    cleft = 1.95d0
    cright = 2.00d0
    do jit = 1, 100 ! j iterations, 'it' == 'iteration'
        cmidl = (cleft + cright)/2.0d0
        call cvalue(cleft, aleft, bleft, cvleft)
        !       print*, 'crvfit iter ', jit
        ! c print*, cleft, aleft, bleft, cvleft
        call cvalue(cright, aright, bright, cvright)
        !       print*, cright, aright, bright, cvright
        call cvalue(cmidl, amidl, bmidl, cvmidl)
        !       print*, cmidl, amidl, bmidl,  cvmidl
        if((cvleft * cvmidl).lt.0.0d0)then
            cright = cmidl
        else
            cleft = cmidl
        endif
    enddo
    af = amidl
    bf = bmidl
    cf = cmidl
    return
end subroutine crvfit


! Also has side-effects, assigns to passed in args
! calculates:
!   see hand-written notes
!
! args:
!   `cfit` seems to be a starting point, not modified
!   `afit` is just the returned, fit a-value
!   `bfit` is just the returned, fit b-value
!   `cval` is just the returned, fit c-value
!
!
subroutine cvalue(cfit, afit, bfit, cval)
    implicit real * 8(a - h, o - z)
    ! number degrees freedom?
    parameter(ndf = 69)  ! 69 for rest,  116 for sp1

    dimension wret(ndf)
    common /param/ wret     ! the data being fit to / having sums calculated
    common /param1/ power   ! 3.35, a global

    ! variables that are iteratively modified in the loop each time
    ! 'sm' == 'sum'
    smy = 0.0d0     !
    sme2cx = 0.0d0  ! e^(2cx)
    smyec = 0.0d0   !
    smec = 0.0d0
    smyxec = 0.0d0
    smxec = 0.0d0
    smxe2c = 0.0d0

    nstart = 1      !       nlast = 0.9d0*(ndf-6)
    nlast = (ndf)
    ndata = nlast - nstart+1
    do i = nstart, nlast
        xi = wret(i) ! fit the "resting" data loaded from patients
        yi = dble(i) ! convert index to double

        ! 'xp' == 'x power' ?
        xpi = exp(-cfit * (xi**(1.0/power))) ! change 5 to 3.35 for sp1
        xp2i = xpi * xpi ! {x^2}_i maybe?

        smy = smy + yi
        sme2cx = sme2cx + xp2i
        smyec = smyec + yi * xpi
        smec = smec + xpi
        smyxec = smyxec + yi * xi * xpi
        smxec = smxec + xi * xpi
        smxe2c = smxe2c + xi * xp2i
    enddo

    !       den = (ndf-6)*sme2cx-smec*smec
    den = ndata * sme2cx - smec * smec ! den == denominator
    afit = (smy * sme2cx - smyec * smec)/den
    !       bfit = (smy*smec-(ndf-6)*smyec)/den
    bfit = (smy * smec - ndata * smyec)/den
    cval = smyxec - (afit * smxec - bfit * smxe2c)

    return
end subroutine cvalue
