# Terminology

* we say a matrix is *drawn from* an ensemble, e.g. the GOE
* GOE - actually only contains real, symmetric matrices, not orthogonal

# Guhr, ..., Mehta (1998)
> Using this approximation, Berry [57] showedthat for classically
> chaotic systems, the ∆3 defined in Eq. (2.10) above has the
> samelogarithmic dependence on the interval length L as for RMT. His
> argument also showed a limita-tion of RMT: The correspondence between
> chaotic systems and RMT applies only up to a maximum L value defined
> by the shortest classical periodic orbit. Beyond this value, ∆3
> becomes constant for dynamical systems.

## Nearest Neighbour Spacing Distribution
```python
a_GOE, b_GOE = pi/2, pi/4
def p_GOE(s):
  return a_GOE * (s ** 1) * exp(-b_GOE * s ** 2)

a_GUE, b_GUE = 32/(pi**2), 4/pi
def p_GUE(s):
  return a_GUE * (s ** 1) * exp(-b_GUE * s ** 2)

a_GSE, b_GSE = 262144/(729*(pi**3)), 64/(9*pi)
def p_GSE(s):
  return a_GSE * (s ** 1) * exp(-b_GSE * s ** 2)
```

## Level Number Variance (∑²)
Algorithm for calculating sigma^2(L)
$\Sigma^2(L) = \underset{c}{\mathit{Ave}}( \mathcal{N}^2(L, c)) - [\underset{c}{\mathit{Ave}}( \mathcal{N}(L, c))]^2$
where $\mathcal{N}(L, c)$ is the number of levels (unfolded eigenvalues) in $[c, c+L]$
```python
# counts the number of levels (unfolded eigenvalues) on [start, start + L]
def num_levels(start, L, unfolded):
  return len(unfolded[unfolded >= start && unfolded <= (start + L)])

# start points can be chosen randomly or exhaustively, but you need to
# do a lot to get an average
# you definitely need to iterate through all values of L though
def sigma_2_iter(start, unfolded):
  L = np.random.uniform(0, len(unfolded))
  levels = num_levels()

```
For "large" L, and where 𝛾 = 0.5772... is Euler's (gamma) constant (`np.euler_gamma` in numpy),
we get the approximations:
* Poisson: ∑²(L) = L
* Harmonic Oscillator: ∑²(L) = 0
* GOE: `∑²(L) = (2/(np.pi**2)) * (np.log(2*np.pi*L) + 𝛾 + 1 - (np.pi**2)/8)`
* GUE: `∑²(L) = (1/(np.pi**2)) * (np.log(2*np.pi*L) + 𝛾 + 1)`
* GSE: `∑²(L) = (1/(2*(np.pi**2))) * (np.log(4*np.pi*L) + 𝛾 + 1 + (np.pi**2)/8)`

## Spectral Rigidity (∆₃)

For "large" L, and where 𝛾 = 0.5772... is Euler's constant,
we get the approximations:
* Poisson: ∆₃(L) = L/15
* Harmonic Oscillator: ∆₃(L) = L/12
* GOE: `∆₃(L) = (1/(pi**2)) * (ln(2*pi*L) + 𝛾 - 5/4 - (pi**2)/8)`
* GUE: `∆₃(L) = (1/(2*(pi**2))) * (ln(2*pi*L) + 𝛾 - 5/4)`
* GSE: `∆₃(L) = (1/(4*(pi**2))) * (ln(4*pi*L) + 𝛾 - 5/4 + (pi**2)/8)`
