import pickle
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

import seaborn as sbn

from numpy import ndarray
from pathlib import Path
from pprint import pprint
from tqdm import tqdm
from tqdm.auto import trange

from typing import Any, Dict, List, Optional, Tuple, Union

from empyricalRMT.eigenvalues import Eigenvalues
from empyricalRMT.ensemble import GOE, Poisson

from simulate import precompute_simulated, N, T, NOISE, GROUPSIZE

DATA_ROOT = Path(__file__).resolve().parent / "data"

eigs_all = precompute_simulated()
