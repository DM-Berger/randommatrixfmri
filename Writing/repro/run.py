import argparse
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sbn
import sys

from glob import glob
from pathlib import Path
from typing import Any, List
from warnings import filterwarnings

from _data_constants import DATA_ROOT, DATASETS, DATASETS_FULLPRE
from _filenames import preview_precompute_outpaths
from _precompute import precompute_dataset
from _utilities import _percentile_boot
from group_compare import Pairing, plot_brody, plot_largest, plot_marchenko
from summarize import compute_all_diffs_dfs, compute_all_preds_df, supplement_stat_dfs


def res(string: str) -> str:
    return str(Path(string).resolve())


def trim_parse(string: str) -> str:
    s = string.replace(" ", "")
    if s[0] != "(" or s[-1] != ")" or s.find(".") != -1:
        raise ValueError("`trim_indices` must by tuple of ints of length 2.")
    return str(s)


def smoother_parse(string: str) -> str:
    if string != "poly":
        raise ValueError("`smoother` argument currently only implemented for 'poly'.")
    return "poly"


def degree_parse(string: str) -> int:
    try:
        deg = int(string)
    except BaseException as e:
        raise ValueError("`degree` must be in int.") from e
    return deg


def bool_parse(string: str) -> bool:
    lower = string.lower()
    if lower == "true" or lower == "t":
        return True
    if lower == "false" or lower == "f":
        return False
    return bool(string)


def brody_parse(string: str) -> str:
    if string in ["mle", "spacing"]:
        return string
    raise ValueError("Brody fit method must be either 'mle' or 'spacing'.")


parser = argparse.ArgumentParser(description="Run with RMT args")
parser.add_argument(
    "-t",
    "--trim",
    metavar="<trim indices>",
    type=trim_parse,
    nargs=1,
    action="store",
    default="(1,-1)",
)
parser.add_argument(
    "-s",
    "--smoother",
    metavar="<smoother>",
    type=smoother_parse,
    nargs=1,
    action="store",
    default="poly",
)
parser.add_argument(
    "-d",
    "--degree",
    metavar="<smoother degree>",
    type=degree_parse,
    nargs=1,
    action="store",
    default=7,
)
parser.add_argument(
    "--detrend", metavar="<detrend>", type=bool_parse, nargs="?", action="store", default=False
)
parser.add_argument(
    "-b", "--brody", metavar="<brody>", type=brody_parse, nargs=1, action="store", default="mle"
)
parser.add_argument(
    "--normalize", metavar="<normalize>", type=bool_parse, nargs="?", action="store", default=False
)
parser.add_argument("--fullpre", action="store_true")

args = parser.parse_args()

# TRIM_IDX = "(1,-1)"  # must be indices of trims, tuple, no spaces
# UNFOLD_ARGS = {"smoother": "poly", "degree": 7, "detrend": False}
# LEVELVAR_ARGS = {"L": np.arange(0.5, 10, 0.1), "tol": 0.001, "max_L_iters": 50000}
# RIGIDITY_ARGS = {"L": np.arange(2, 20, 0.5)}
# BRODY_ARGS = {"method": "mle"}

trim = args.trim
smoother = args.smoother
degree = args.degree
brody = args.brody
normalize = args.normalize
is_fullpre = args.fullpre

TRIM_IDX = trim[0] if isinstance(trim, list) else trim
UNFOLD_ARGS = {
    "smoother": smoother[0] if isinstance(smoother, list) else smoother,
    "degree": degree[0] if isinstance(degree, list) else degree,
    "detrend": args.detrend,
}
LEVELVAR_ARGS = {"L": np.arange(0.5, 10, 0.1), "tol": 0.001, "max_L_iters": 50000}
RIGIDITY_ARGS = {"L": np.arange(2, 20, 0.5)}
BRODY_ARGS = {"method": brody[0] if isinstance(brody, list) else brody}
NORMALIZE = normalize[0] if isinstance(normalize, list) else normalize
FULLPRE = is_fullpre[0] if isinstance(is_fullpre, list) else is_fullpre

# yes, this is grotesque, but sometimes you need some damn singletons
# fmt: off
class Args:
    exists = False
    def __init__(self): # noqa
        if Args.exists: raise RuntimeError("Args object already exists.") # noqa

    @property
    def trim(self): return TRIM_IDX # noqa
    @property
    def unfold(self): return UNFOLD_ARGS # noqa
    @property
    def levelvar(self): return LEVELVAR_ARGS # noqa
    @property
    def rigidity(self): return RIGIDITY_ARGS # noqa
    @property
    def brody(self): return BRODY_ARGS # noqa
    @property
    def normalize(self): return NORMALIZE # noqa
    @property
    def fullpre(self): return FULLPRE # noqa

    def print(self):
        from pprint import pprint
        print("=" * 80)
        print("Performing calculations for args:\n")
        print("TRIM_ARGS:      ", end="")
        pprint(self.trim)
        print("UNFOLD_ARGS:    ", end="")
        pprint(self.unfold)

        rig_min, rig_max = self.rigidity["L"].min(), np.round(self.rigidity["L"].max(), 2)
        rig_diff = np.round(np.diff(self.rigidity["L"])[0], 2)
        print(f"RIGIDITY_ARGS:  L=np.arange({rig_min}, {rig_max}, {rig_diff})")

        var_min, var_max = self.levelvar["L"].min(), np.round(self.levelvar["L"].max(), 2)
        var_diff = np.round(np.diff(self.levelvar["L"])[0], 2)
        tol, iters = self.levelvar["tol"], self.levelvar["max_L_iters"]
        print(f"LEVELVAR_ARGS:  L=np.arange({var_min}, {var_max}, {var_diff}), tol={tol}, max_L_iters={iters}")
        print(f"BRODY_ARGS:     {self.brody['method']}")
        print(f"NORMALIZE:      {self.normalize}")
        print(f"FULLPRE:        {self.fullpre}")
        print("")
        print("=" * 80)

    def cmd(self):
        smoother = self.unfold["smoother"]
        if isinstance(smoother, list):
            smoother = smoother[0]
        brody = self.brody["method"]
        if isinstance(brody, list):
            brody = brody[0]
        args = [
            f"--trim='{self.trim}'",
            f"--smoother='{smoother}'",
            f"--degree='{self.unfold['degree']}'",
            f"--brody='{brody}'",
        ]
        if self.unfold["detrend"]:
            args.append(f"--detrend")
        if self.normalize:
            args.append(f"--normalize")
        if self.fullpre:
            args.append(f"--fullpre")
        print(f"python3 run.py {' '.join(args)};")

ARGS = Args() # noqa
# fmt: on
# compute_all_preds_df(ARGS, NORMALIZE, silent=True)


def plot_learning_fails(args: Any):
    pass


def plot_pred_rigidity(
    args: Any,
    dataset_name: str,
    comparison: str,
    unfold: List[int] = [5, 7, 9, 11, 13],
    silent: bool = False,
    force: bool = False,
) -> None:
    global TRIM_IDX
    global NORMALIZE
    global UNFOLD_ARGS
    global ARGS
    global FULLPRE
    FULLPRE = True
    for TRIM_IDX in ["(1,-1)", "(1,-20)"]:
        all_pairings = []
        for NORMALIZE in [False]:
            for UNFOLD_ARGS["degree"] in unfold:
                pairings = Pairing.pairings_from_precomputed(dataset_name, args)
                pairing = list(filter(lambda p: p.label == comparison, pairings))
                if len(pairing) != 1:
                    raise ValueError("Too many pairings, something is wrong.")
                all_pairings.append(pairing[0])
        g1, _, g2 = all_pairings[0].label.split("_")  # groupnames
        fig: plt.Figure
        fig, axes = plt.subplots(nrows=1, ncols=len(all_pairings), sharex=True)
        for i, pairing in enumerate(all_pairings):
            ax: plt.Axes = axes.flat[i]
            df1 = pd.read_pickle(pairing.rigidity[0]).set_index("L")
            df2 = pd.read_pickle(pairing.rigidity[1]).set_index("L")
            boots1 = _percentile_boot(df1)
            boots2 = _percentile_boot(df2)
            sbn.lineplot(x=df1.index, y=boots1["mean"], color="#FD8208", label=g1, ax=ax)
            ax.fill_between(
                x=df1.index, y1=boots1["low"], y2=boots1["high"], color="#FD8208", alpha=0.3
            )
            sbn.lineplot(x=df2.index, y=boots2["mean"], color="#000000", label=g2, ax=ax)
            ax.fill_between(
                x=df2.index, y1=boots2["low"], y2=boots2["high"], color="#000000", alpha=0.3
            )
            ax.set_title(f"Unfolding Degree {unfold[i]}")
            ax.set_xlabel("")
            ax.set_ylabel("")
        fig.text(0.5, 0.04, "L", ha="center", va="center")  # xlabel
        fig.text(
            0.1,
            0.5,
            "∆₃(L)",
            ha="center",
            va="center",
            rotation="vertical",
            fontdict={"fontname": "DejaVu Sans"},
        )  # ylabel
        plt.suptitle(f"{dataset_name} {TRIM_IDX} - Spectral Rigidity")
        plt.show(block=False)


def plot_pred_levelvar(
    args: Any,
    dataset_name: str,
    comparison: str,
    unfold: List[int] = [5, 7, 9, 11, 13],
    silent: bool = False,
    force: bool = False,
) -> None:
    global TRIM_IDX
    global NORMALIZE
    global UNFOLD_ARGS
    global ARGS
    global FULLPRE
    FULLPRE = True
    for TRIM_IDX in ["(1,-1)", "(1,-20)"]:
        all_pairings = []
        for NORMALIZE in [False]:
            for UNFOLD_ARGS["degree"] in unfold:
                pairings = Pairing.pairings_from_precomputed(dataset_name, args)
                pairing = list(filter(lambda p: p.label == comparison, pairings))
                if len(pairing) != 1:
                    raise ValueError("Too many pairings, something is wrong.")
                all_pairings.append(pairing[0])
        g1, _, g2 = all_pairings[0].label.split("_")  # groupnames
        fig: plt.Figure
        fig, axes = plt.subplots(nrows=1, ncols=len(all_pairings), sharex=True)
        for i, pairing in enumerate(all_pairings):
            ax: plt.Axes = axes.flat[i]
            df1 = pd.read_pickle(pairing.levelvar[0]).set_index("L")
            df2 = pd.read_pickle(pairing.levelvar[1]).set_index("L")
            boots1 = _percentile_boot(df1)
            boots2 = _percentile_boot(df2)
            sbn.lineplot(x=df1.index, y=boots1["mean"], color="#FD8208", label=g1, ax=ax)
            ax.fill_between(
                x=df1.index, y1=boots1["low"], y2=boots1["high"], color="#FD8208", alpha=0.3
            )
            sbn.lineplot(x=df2.index, y=boots2["mean"], color="#000000", label=g2, ax=ax)
            ax.fill_between(
                x=df2.index, y1=boots2["low"], y2=boots2["high"], color="#000000", alpha=0.3
            )
            ax.set_title(f"Unfolding Degree {unfold[i]}")
            ax.set_xlabel("")
            ax.set_ylabel("")
        fig.text(0.5, 0.04, "L", ha="center", va="center")  # xlabel
        fig.text(
            0.1,
            0.5,
            "∆₃(L)",
            ha="center",
            va="center",
            rotation="vertical",
            fontdict={"fontname": "DejaVu Sans"},
        )  # ylabel
        plt.suptitle(f"{dataset_name} {TRIM_IDX} - Level Number Variance")
        plt.show(block=False)


def make_pred_hists(
    args: Any,
    density: bool = True,
    normalize: bool = False,
    silent: bool = False,
    force: bool = False,
) -> Path:
    global TRIM_IDX
    global NORMALIZE
    global UNFOLD_ARGS
    global ARGS
    global FULLPRE
    FULLPRE = True
    dfs = []

    def hist_over_trim(trim: str, unfold=[5, 7, 9, 11, 13], normalize=normalize):
        global TRIM_IDX
        global NORMALIZE
        global UNFOLD_ARGS
        global ARGS
        global FULLPRE

        for TRIM_IDX in [trim]:
            for NORMALIZE in [normalize]:
                for UNFOLD_ARGS["degree"] in unfold:
                    supplemented = supplement_stat_dfs(
                        diffs=None, preds=compute_all_preds_df(args, silent=True)
                    )[1]
                    dfs.append(pd.read_csv(supplemented))
        FEATURES = [
            "Raw Eigs",
            # "Largest",
            # "Largest20",
            # "Noise",
            # "Noise (shift)",
            # "Brody",
            # "Rigidity",
            # "Levelvar",
        ]
        HCOLORS = [
            "#777777",  # Raw Eigs
            # "#000000",  # Raw Eigs
            # "#c10000",  # Largest
            # "#a80000",  # Largest20
            # "#06B93A",  # Noise
            # "#058C2C",  # Noise (shift)
            # "#EA00FF",  # brody
            # "#FD8208",  # rigidity
            # "#D97007",  # levelvar
        ]

        orig = pd.concat(dfs)
        hist_info, bins_all, guesses, titles = [], [], [], []
        for dataset in orig["Dataset"].unique():
            by_dataset = orig[orig["Dataset"] == dataset]
            for comparison in by_dataset["Comparison"].unique():
                all_algo_compare = by_dataset[by_dataset["Comparison"] == comparison]
                hist_data = all_algo_compare[FEATURES]
                if len(hist_data) == 0:
                    continue
                hmin, hmax = hist_data.min().min(), hist_data.max().max()
                bins = np.linspace(hmin, hmax, 8)
                hist_info.append(hist_data)
                bins_all.append(bins)
                guesses.append(all_algo_compare["Guess"].iloc[0])
                titles.append(f"{dataset}--{comparison}")
        fig, axes = plt.subplots(nrows=3, ncols=6, sharex=False)
        for i, (hist_data, bins, guess, title) in enumerate(
            zip(hist_info, bins_all, guesses, titles)
        ):
            sbn.set_style("ticks")
            sbn.set_palette("Accent")
            ax: plt.Axes = axes.flat[i]
            ax.hist(
                hist_data.T,
                # hist_data,
                bins=bins,
                # bins=20,
                stacked=True,
                density=density,
                histtype="bar",
                label=hist_data.columns,
                color=HCOLORS,
            )
            ax.axvline(x=guess, color="black", label="Guess")
            if i == 0:
                ax.legend()
            ax.set_title(title, fontdict={"fontsize": 8})
        fig.text(0.5, 0.04, "Feature Prediction Accuracy", ha="center", va="center")  # xlabel
        fig.text(
            0.1,
            0.5,
            "Density" if density else "Frequency",
            ha="center",
            va="center",
            rotation="vertical",
        )  # ylabel
        f = " (fullpre)" if FULLPRE else ""
        n = " (normed)" if NORMALIZE else ""
        fig.suptitle(f"Trim {trim} unfolds={unfold}{f}{n}")
        fig.subplots_adjust(hspace=0.3, wspace=0.3)
        plt.show(block=False)

    for FULLPRE in [True]:
        # for unfold in [[5, 7], [11, 13]]:
        for unfold in [[5, 7, 9, 11, 13]]:
            hist_over_trim(trim="(1,-1)", normalize=normalize, unfold=unfold)
            hist_over_trim(trim="(1,-20)", normalize=normalize, unfold=unfold)


def all_pred_means_by_algo(
    args: Any,
    dataset_name: str = None,
    comparison: str = None,
    subtract_guess: str = True,
    fullpre: bool = True,
    normalize: bool = False,
    silent: bool = False,
    force: bool = False,
) -> Path:
    global TRIM_IDX
    global NORMALIZE
    global UNFOLD_ARGS
    global ARGS
    global FULLPRE
    FULLPRE = fullpre

    UNFOLD = [5, 7, 9, 11, 13]

    dfs = []
    for TRIM_IDX in ["(1,-1)", "(1,-20)"]:
        for NORMALIZE in [normalize]:
            for UNFOLD_ARGS["degree"] in UNFOLD:
                supplemented = supplement_stat_dfs(
                    diffs=None, preds=compute_all_preds_df(args, silent=silent)
                )[1]
                df = pd.read_csv(supplemented)
                df["Degree"] = UNFOLD_ARGS["degree"]
                df["Trim"] = TRIM_IDX
                dfs.append(df)

    compare: pd.DataFrame = pd.concat(dfs)
    features = [
        "Raw Eigs",
        "Largest20",
        "Largest",
        "Noise",
        "Noise (shift)",
        "Rigidity",
        "Levelvar",
    ]
    compare = compare[
        [
            "Dataset",
            "Algorithm",
            "Comparison",
            "Degree",
            "Trim",
            "Raw Eigs",
            "Largest20",
            "Largest",
            "Noise",
            "Noise (shift)",
            "Rigidity",
            "Levelvar",
            "Guess",
        ]
    ]
    if dataset_name is not None:
        compare = compare[compare["Dataset"] == dataset_name]
    if comparison is not None:
        compare = compare[compare["Comparison"] == comparison]

    algo_eigs, algo_20, algo_rig, algo_var, algo_largest, algo_noise, algo_noise_shift = [
        pd.DataFrame(dtype=float) for _ in range(7)
    ]
    for algo in compare["Algorithm"].unique():
        # print(algo)
        alg_compare = compare[compare["Algorithm"] == algo]
        if subtract_guess:
            alg_compare = alg_compare[features].apply(lambda col: col - alg_compare["Guess"])
        desc = alg_compare.describe().drop(labels="count", axis=0)
        if algo == "Logistic Regression":
            algo = "LR"
        # guess = desc["Guess"] if subtract_guess else desc["Guess"] - desc["Guess"]
        algo_eigs[algo] = desc["Raw Eigs"].rename(columns={"Raw Eigs": algo})
        algo_20[algo] = desc["Largest20"].rename(columns={"Largest20": algo})
        algo_rig[algo] = desc["Rigidity"].rename(columns={"Rigidity": algo})
        algo_var[algo] = desc["Levelvar"].rename(columns={"Levelvar": algo})
        algo_largest[algo] = desc["Largest"].rename(columns={"Largest": algo})
        algo_noise[algo] = desc["Noise"].rename(columns={"Noise": algo})
        algo_noise_shift[algo] = desc["Noise (shift)"].rename(columns={"Noise (shift)": algo})
        # print(compare[compare["Algorithm"] == algo].describe())
        # print("\n\n")
        # algo_rig["Guess"] =
    d = f"{dataset_name}_" if dataset_name is not None else ""
    c = f"{comparison}_" if comparison is not None else ""
    f = "_fullpre" if FULLPRE else ""
    n = "_normed" if normalize else ""
    g = "-guess" if subtract_guess else ""
    algo_eigs.to_csv(DATA_ROOT / f"{d}{c}all_preds_eigs_by_algo{f}{n}{g}.csv")
    algo_20.to_csv(DATA_ROOT / f"{d}{c}all_preds_largest20_by_algo{f}{n}{g}.csv")
    algo_rig.to_csv(DATA_ROOT / f"{d}{c}all_preds_rigidity_by_algo{f}{n}{g}.csv")
    algo_var.to_csv(DATA_ROOT / f"{d}{c}all_preds_levelvar_by_algo{f}{n}{g}.csv")
    algo_largest.to_csv(DATA_ROOT / f"{d}{c}all_preds_largest_by_algo{f}{n}{g}.csv")
    algo_noise.to_csv(DATA_ROOT / f"{d}{c}all_preds_noise_by_algo{f}{n}{g}.csv")
    algo_noise_shift.to_csv(DATA_ROOT / f"{d}{c}all_preds_noise_shift_by_algo{f}{n}{g}.csv")


def make_pred_means(
    args: Any,
    dataset_name: str,
    comparison: str,
    subtract_guess: bool = False,
    silent: bool = False,
    force: bool = False,
) -> Path:
    global TRIM_IDX
    global NORMALIZE
    global UNFOLD_ARGS
    global ARGS
    global FULLPRE
    FULLPRE = True

    UNFOLD = [5, 7, 9, 11, 13]
    normalize = False

    dfs = []
    for TRIM_IDX in ["(1,-1)", "(1,-20)"]:
        for NORMALIZE in [normalize]:
            for UNFOLD_ARGS["degree"] in UNFOLD:
                supplemented = supplement_stat_dfs(
                    diffs=None, preds=compute_all_preds_df(args, silent=True)
                )[1]
                df = pd.read_csv(supplemented)
                df["Degree"] = UNFOLD_ARGS["degree"]
                df["Trim"] = TRIM_IDX
                dfs.append(df)

    orig: pd.DataFrame = pd.concat(dfs)
    data = orig[orig["Dataset"] == dataset_name]
    compare = data[data["Comparison"] == comparison]
    compare = compare[["Algorithm", "Degree", "Trim", "Rigidity", "Levelvar", "Guess"]]
    # print(compare[["Algorithm", "Degree", "Trim", "Rigidity", "Levelvar", "Guess"]])
    # print("Rigidity:")
    # print(compare["Rigidity"].describe())
    # print("Levelvar:")
    # print(compare["Levelvar"].describe())
    algo_rig, algo_var = pd.DataFrame(), pd.DataFrame()
    for algo in compare["Algorithm"].unique():
        # print(algo)
        desc = compare[compare["Algorithm"] == algo].describe()
        if subtract_guess:
            algo_rig[algo] = desc["Rigidity"].rename(columns={"Rigidity": algo}) - desc["Guess"]
            algo_rig[algo] = desc["Rigidity"].rename(columns={"Rigidity": algo}) - desc["Guess"]
        else:
            algo_rig[algo] = desc["Rigidity"].rename(columns={"Rigidity": algo})
            algo_var[algo] = desc["Levelvar"].rename(columns={"Levelvar": algo})
        # print(compare[compare["Algorithm"] == algo].describe())
        # print("\n\n")
    guess_label = "-guess" if subtract_guess else ""
    rig_out = DATA_ROOT / f"{dataset_name}_{comparison}_rigidity_by_algo{guess_label}.csv"
    var_out = DATA_ROOT / f"{dataset_name}_{comparison}_levelvar_by_algo{guess_label}.csv"
    algo_rig.to_csv(rig_out)
    algo_var.to_csv(var_out)
    print(
        f"Saved {dataset_name} {comparison} rigidity predictions by algorithm to {rig_out.relative_to(DATA_ROOT)}"
    )
    print(
        f"Saved {dataset_name} {comparison} levelvar predictions by algorithm to {var_out.relative_to(DATA_ROOT)}"
    )


def make_marchenko_plots(args: Any, shifted: bool = False):
    global TRIM_IDX
    global ARGS
    global FULLPRE
    for FULLPRE in [True, False]:
        for NORMALIZE in [False]:
            datasets_all = DATASETS_FULLPRE if args.fullpre else DATASETS
            datasets = {}
            for dataset_name, dataset in datasets_all.items():
                if dataset_name == "SINGLE_SUBJECT":
                    continue
                datasets[dataset_name] = dataset

            # print(len(datasets))  # 12
            # sys.exit(1)

            fig: plt.Figure
            fig, axes = plt.subplots(nrows=4, ncols=3)
            suptitle = f"{'Shifted ' if shifted else ''}Marchenko Noise Ratio {'(preprocessed)' if FULLPRE else ''}"
            for i, dataset_name in enumerate(datasets):
                dfs = []
                for groupname, observables in precompute_dataset(
                    dataset_name, args, silent=True
                ).items():
                    df_full = pd.read_pickle(observables["marchenko"])
                    df = pd.DataFrame(
                        df_full.loc["noise_ratio_shifted" if shifted else "noise_ratio", :]
                    )
                    df["subgroup"] = [groupname for _ in range(len(df))]
                    dfs.append(df)
                df = pd.concat(dfs)

                sbn.set_context("paper")
                sbn.set_style("ticks")
                # args_prefix = argstrings_from_args(args)[0]
                dname = dataset_name.upper()
                # prefix = f"{dname}_{args_prefix}_{'fullpre_' if args.fullpre else ''}"
                title = f"{dname}"

                with sbn.axes_style("ticks"):
                    subfontsize = 10
                    fontsize = 12
                    ax: plt.Axes = axes.flat[i]
                    sbn.violinplot(
                        x="subgroup",
                        y=f"noise_ratio{'_shifted' if shifted else ''}",
                        data=df,
                        ax=ax,
                    )
                    sbn.despine(offset=10, trim=True, ax=ax)
                    ax.set_title(title, fontdict={"fontsize": fontsize})
                    ax.set_xlabel("", fontdict={"fontsize": subfontsize})
                    ax.set_ylabel("", fontdict={"fontsize": subfontsize})
            fig.suptitle(suptitle)
            fig.text(x=0.5, y=0.04, s="Subgroup", ha="center", va="center")  # xlabel
            fig.text(
                x=0.05, y=0.5, s="Noise Proportion", ha="center", va="center", rotation="vertical"
            )  # ylabel
            fig.subplots_adjust(hspace=0.48, wspace=0.3)
            # sbn.despine(ax=axes.flat[-1], trim=True)
            fig.delaxes(ax=axes.flat[-1])
            plt.show(block=False)

    plt.show()

    # plt.show()
    # plt.close()
    # else:
    # out = outdir / f"{prefix}marchenko.png"
    # plt.gcf().set_size_inches(w=8, h=8)
    # plt.savefig(out)
    # plt.close()
    # print(f"Marchenko plot saved to {relpath(out)}")


def make_largest_plots(args: Any):
    global TRIM_IDX
    global ARGS
    global FULLPRE
    for FULLPRE in [True, False]:
        for NORMALIZE in [False]:
            datasets_all = DATASETS_FULLPRE if args.fullpre else DATASETS
            datasets = {}
            for dataset_name, dataset in datasets_all.items():
                if dataset_name == "SINGLE_SUBJECT":
                    continue
                datasets[dataset_name] = dataset

            # print(len(datasets))  # 12
            # sys.exit(1)

            fig: plt.Figure
            fig, axes = plt.subplots(nrows=4, ncols=3)
            suptitle = f"Largest Eigenvalue{' (preprocessed)' if FULLPRE else ''}"
            for i, dataset_name in enumerate(datasets):
                dfs = []
                for groupname, observables in precompute_dataset(
                    dataset_name, args, silent=True
                ).items():
                    df_full = pd.read_pickle(observables["largest"])
                    df = pd.DataFrame(df_full.loc["largest", :], dtype=float)
                    df["subgroup"] = [groupname for _ in range(len(df))]
                    dfs.append(df)
                df = pd.concat(dfs)

                sbn.set_context("paper")
                sbn.set_style("ticks")
                dname = dataset_name.upper()
                title = f"{dname}"

                with sbn.axes_style("ticks"):
                    subfontsize = 10
                    fontsize = 12
                    ax: plt.Axes = axes.flat[i]
                    sbn.violinplot(x="subgroup", y="largest", data=df, ax=ax)
                    sbn.despine(offset=10, trim=True, ax=ax)
                    ax.set_title(title, fontdict={"fontsize": fontsize})
                    ax.set_xlabel("", fontdict={"fontsize": subfontsize})
                    ax.set_ylabel("", fontdict={"fontsize": subfontsize})
            fig.suptitle(suptitle)
            fig.text(x=0.5, y=0.04, s="Subgroup", ha="center", va="center")  # xlabel
            fig.text(
                x=0.05, y=0.5, s="Magnitude", ha="center", va="center", rotation="vertical"
            )  # ylabel
            fig.subplots_adjust(hspace=0.48, wspace=0.3)
            # sbn.despine(ax=axes.flat[-1], trim=True)
            fig.delaxes(ax=axes.flat[-1])
            plt.show(block=False)

    plt.show()

    # plt.show()
    # plt.close()
    # else:
    # out = outdir / f"{prefix}marchenko.png"
    # plt.gcf().set_size_inches(w=8, h=8)
    # plt.savefig(out)
    # plt.close()
    # print(f"Marchenko plot saved to {relpath(out)}")


def get_cmds():
    global TRIM_IDX
    global NORMALIZE
    global UNFOLD_ARGS
    global ARGS
    global FULLPRE
    FULLPRE = True
    for TRIM_IDX in ["(1,-1)", "(1,-20)"]:
        for NORMALIZE in [False]:
            for UNFOLD_ARGS["degree"] in [5, 7, 9, 11, 13]:
                # ARGS.print()
                ARGS.cmd()
                # plot_marchenko(ARGS)
                # plot_brody(ARGS)
                plot_largest(ARGS)
                # compute_all_diffs_dfs(ARGS, NORMALIZE, silent=True)
                # diffs = compute_all_diffs_dfs(ARGS, silent=True)
                # supplement_stat_dfs(diffs=diffs)


filterwarnings("ignore", category=RuntimeWarning)
filterwarnings("ignore", category=FutureWarning)
filterwarnings("ignore", category=np.RankWarning)

# preds = compute_all_preds_df(ARGS, silent=True)
# supplement_stat_dfs(preds=preds)
# get_cmds()
# all_pred_means_by_algo(ARGS, "PSYCH_VIGILANCE_SES-1", "high_v_low", subtract_guess=False, fullpre=True, normalize=True, silent=True)
# all_pred_means_by_algo(ARGS, "PSYCH_VIGILANCE_SES-2", "high_v_low", subtract_guess=False, fullpre=True,  normalize=True, silent=True)
# all_pred_means_by_algo(ARGS, None, fullpre=False, silent=True)
# all_pred_means_by_algo(
#     ARGS, "OSTEO", "duloxetine_v_nopain", subtract_guess=True, fullpre=True, silent=True
# )
# all_pred_means_by_algo(
#     ARGS, "OSTEO", "duloxetine_v_nopain", subtract_guess=False, fullpre=True, silent=True
# )
# all_pred_means_by_algo(ARGS, "OSTEO", "duloxetine_v_nopain", subtract_guess=True, fullpre=True, normalize=False, silent=True)
# all_pred_means_by_algo(
#     ARGS, "REFLECT_INTERLEAVED", subtract_guess=False, fullpre=True, normalize=True, silent=False
# )
# all_pred_means_by_algo(
#     ARGS,
#     "PARKINSONS",
#     "control_v_parkinsons",
#     subtract_guess=False,
#     fullpre=True,
#     normalize=True,
#     silent=True,
# )
# all_pred_means_by_algo(
#     ARGS,
#     "PARKINSONS",
#     "control_v_parkinsons",
#     subtract_guess=False,
#     fullpre=True,
#     normalize=False,
#     silent=True,
# )
all_pred_means_by_algo(ARGS, subtract_guess=True, normalize=True, fullpre=True, silent=True)
all_pred_means_by_algo(ARGS, subtract_guess=True, normalize=False, fullpre=True, silent=True)
# make_pred_means(ARGS, dataset_name="OSTEO", comparison="duloxetine_v_nopain", silent=True)
# make_pred_hists(ARGS, density=True, silent=True)
# make_pred_hists(ARGS, normalize=True, density=True, silent=True)
# make_pred_hists(ARGS, normalize=False, density=True, silent=True)
# make_marchenko_plots(ARGS, shifted=True)
# make_largest_plots(ARGS)
# plot_pred_rigidity(ARGS, "OSTEO", "duloxetine_v_nopain", silent=True, force=False)
# plot_pred_rigidity(ARGS, "PSYCH_TASK_ATTENTION_SES-1", "high_v_low", silent=True, force=False)
# plot_pred_rigidity(ARGS, "PARKINSONS", "control_v_parkinsons", silent=True, force=False)
# plot_pred_levelvar(ARGS, "PARKINSONS", "control_v_parkinsons", silent=True, force=False)
# plot_pred_levelvar(ARGS, "OSTEO", "duloxetine_v_nopain", silent=True, force=False)
# make_pred_means(ARGS, "OSTEO", "duloxetine_v_nopain", subtract_guess=True, silent=True, force=False)
# make_pred_means(
#     ARGS, "OSTEO", "duloxetine_v_nopain", subtract_guess=False, silent=True, force=False
# )

plt.show()
sys.exit(0)
# preview_precompute_outpaths(ARGS)
# compute_all_preds_df(ARGS, silent=True)

# preds = compute_all_preds_df(ARGS, silent=False)
# supplement_stat_dfs(preds=preds)

# plot_marchenko(ARGS)
# plot_brody(pathdict, title=f"{dataset_name.upper()} - Brody β", outdir=PLOT_OUTDIRS[dataset_name])
# plot_largest(
#     pathdict,
#     title=f"{dataset_name.upper()} - Largest Eigenvalue",
#     outdir=PLOT_OUTDIRS[dataset_name],
# )


# for pairing in pairings:
#     print(pairing.paired_differences(trim_args=TRIM_IDX, unfold_args=UNFOLD_ARGS))
#     pairing.plot_nnsd(
#         trim_args=TRIM_IDX,
#         unfold_args=UNFOLD_ARGS,
#         title=f"{dataset_name} - NNSD",
#         outdir=PLOT_OUTDIRS[dataset_name],
#     )
#     pairing.plot_rigidity(
#         title=f"{dataset_name}: {pairing.subgroup1} v. {pairing.subgroup2} - Rigidity",
#         outdir=PLOT_OUTDIRS[dataset_name],
#     )
#     pairing.plot_levelvar(
#         title=f"{dataset_name}: {pairing.subgroup1} v. {pairing.subgroup2} - Levelvar",
#         outdir=PLOT_OUTDIRS[dataset_name],
#     )
