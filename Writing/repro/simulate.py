import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import pickle
import seaborn as sbn
import warnings

from empyricalRMT.brody import brody_dist, fit_brody
from empyricalRMT.eigenvalues import Eigenvalues
from empyricalRMT.ensemble import GOE, Poisson
from numpy import ndarray
from pandas import DataFrame
from pathlib import Path
from time import ctime
from tqdm import tqdm
from typing import Any, Dict, List, Optional, Tuple
from typing_extensions import Literal

from _filenames import argstrings_from_args
from _utilities import _kde, _percentile_boot


AllEigs = Dict[str, Dict[int, Dict[int, ndarray]]]
SummaryDict = Dict[str, Dict[int, Dict[int, Dict[str, Path]]]]
PlotMode = Literal["save", "show"]

# fmt: off
TRIM_ARGS     = r"(0,-1)"  # must be indices of trims, tuple, no spaces
UNFOLD_ARGS   = {"smoother": "poly", "degree": 7, "detrend": False}
LEVELVAR_ARGS = {"L": np.arange(0.5, 10, 0.1), "tol": 0.001, "max_L_iters": 50000}
RIGIDITY_ARGS = {"L": np.arange(2, 20, 0.5)}
BRODY_ARGS    = {"method": "mle"}
# fmt: on

N = 200_000
T = 300
GROUPSIZE = 20
NOISE = 0.5
CORR_PERCENTS = [10, 25, 50, 75, 90]
N_CLUSTERS = [1, 10, 50, 75, 100, 150, 200, 250, 300, 500]

DATA_ROOT = Path(__file__).resolve().parent / "data"
SUMMARY_OUTDIR = DATA_ROOT / "Simulated" / "rmt" / f"noise_{NOISE}_groupsize{GROUPSIZE}"
PLOT_OUTDIR = DATA_ROOT / "Simulated" / "plots" / f"noise_{NOISE}_groupsize{GROUPSIZE}"
os.makedirs(SUMMARY_OUTDIR, exist_ok=True)
os.makedirs(PLOT_OUTDIR, exist_ok=True)

LOGFILE = DATA_ROOT.parent / f"{ctime().replace(' ', '_').replace(':', '.')}__precompute.log"


def _configure_sbn_style(prepend_black: bool = True) -> None:
    """
    NOTE
    ----
    This must be called every time *before* creating new figs and axes. In order
    to fully ensure all plots get the correct style.
    """
    palette = sbn.color_palette("dark").copy()
    if prepend_black:
        palette.insert(0, (0.0, 0.0, 0.0))
    sbn.set()
    sbn.set_palette(palette)


class Log:
    def __init__(self) -> None:
        self.log: List[str] = []
        if LOGFILE.exists():
            os.remove(LOGFILE)
        self.out = LOGFILE

    def append(self, s: str) -> None:
        self.log.append(s)
        with open(self.out, "a+") as log:
            log.write(f"{s}\n")

    def print(self) -> None:
        print("\n".join(self.log))


def summaryfiles_from_args(
    args: Any, kind: Literal["norm", "unif"], corr: int, clust_size: int
) -> dict:
    raise NotImplementedError("You need to finish the refactor of this from Args change.")
    prefix, labels = argstrings_from_args(args)
    sim_prefix = f"{kind}_{corr}-percent-corr_clust-{clust_size}"
    # fmt: off
    largest_out   = SUMMARY_OUTDIR / f"{sim_prefix}_largest.zip"
    marchenko_out = SUMMARY_OUTDIR / f"{sim_prefix}_marchenko.zip"
    brody_out     = SUMMARY_OUTDIR / f"{sim_prefix}_{labels['brody']}.zip"
    nnsd_out      = SUMMARY_OUTDIR / f"{sim_prefix}_{prefix}_nnsd.zip"
    rigidity_out  = SUMMARY_OUTDIR / f"{sim_prefix}_{labels['rigidity']}.zip"
    levelvar_out  = SUMMARY_OUTDIR / f"{sim_prefix}_{labels['levelvar']}.zip"
    # fmt: on
    return {
        "largest": largest_out,
        "marchenko": marchenko_out,
        "brody": brody_out,
        "nnsd": nnsd_out,
        "rigidity": rigidity_out,
        "levelvar": levelvar_out,
    }


def make_signals(
    kind: str, corr_percent: int, n_clusters: int, N: int = N, t: int = T, noise: float = NOISE
) -> ndarray:
    clust_size = int(np.floor(((corr_percent / 100) / n_clusters) * N))
    n_correlated = int(n_clusters * clust_size)
    n_iid = int(N - n_correlated)
    bases = None
    if kind == "norm" or kind == "normal":
        bases = np.random.standard_normal([n_clusters, t])
    elif kind == "unif" or kind == "uniform":
        bases = np.random.uniform(0, 1, [n_clusters, t])
    else:
        raise ValueError("Invalid kind.")
    signals = np.empty([N, t])
    for i in range(n_clusters):
        signals[(i) * clust_size : (i + 1) * clust_size, :] = bases[i, :] + np.random.normal(
            0, noise, [clust_size, t]
        )
    if kind == "norm" or kind == "normal":
        signals[-n_iid:, :] = np.random.standard_normal([n_iid, t])
    if kind == "unif" or kind == "uniform":
        signals[-n_iid:, :] = np.random.uniform(0, 1, [n_iid, t])
    return signals


def simulate_groups() -> Dict[str, Dict[int, Dict[int, ndarray]]]:
    """Generate eigenvalue data for a bunch of groups.

    Note:
    -----
    The groups will be:

        - norm: purely random Gaussian entries
        - norm_corr10_1: 10% correlated with 1
        - norm_corr50_1: 50% correlated with 1
        - norm_corr90_1: 90% correlated with 1
        - norm_corr10_10: 10% correlated with 10 norms
        - norm_corr50_10: 50% correlated with 10 norms
        - norm_corr90_10: 90% correlated with 10 norms
        - norm_corr10_100: 10% correlated with 100 norms
        - norm_corr50_100: 50% correlated with 100 norms
        - norm_corr90_100: 90% correlated with 100 norms

        - unif: purely random Uniform(0,1) entries
        - unif_corr10_1: 10% correlated with 1
        - unif_corr50_1: 50% correlated with 1
        - unif_corr90_1: 90% correlated with 1
        - unif_corr10_10: 10% correlated with 10 unifs
        - unif_corr50_10: 50% correlated with 10 unifs
        - unif_corr90_10: 90% correlated with 10 unifs
        - unif_corr10_100: 10% correlated with 100 unifs
        - unif_corr50_100: 50% correlated with 100 unifs
        - unif_corr90_100: 90% correlated with 100 unifs
    """
    os.makedirs(DATA_ROOT / "Simulated" / "norm", exist_ok=True)
    os.makedirs(DATA_ROOT / "Simulated" / "unif", exist_ok=True)

    def desc(kind: str, corr_percent: int, n_clusters: int) -> str:
        if kind == "norm":
            kind = "normally-distributed"
        elif kind == "unif":
            kind == "uniformly-distributed"
        else:
            raise ValueError("Invalid kind.")
        if n_clusters == 0:
            return f"200 000 i.i.d. {kind} signals"
        return f"200 000 {kind} signals, {corr_percent}% correlated across {n_clusters} clusters"

    eigs_all: Dict[str, Dict[int, Dict[int, ndarray]]] = {
        "norm": {
            corr: {size: np.empty([GROUPSIZE, T - 1]) for size in N_CLUSTERS}
            for corr in CORR_PERCENTS
        },
        "unif": {
            corr: {size: np.empty([GROUPSIZE, T - 1]) for size in N_CLUSTERS}
            for corr in CORR_PERCENTS
        },
    }
    for n_clusters in tqdm(N_CLUSTERS, desc="Clusters", total=len(N_CLUSTERS)):
        for corr_percent in tqdm(
            CORR_PERCENTS, desc="Corr. percent", total=len(CORR_PERCENTS), leave=False
        ):
            for kind in tqdm(["norm", "unif"], desc="Kind", total=2, leave=False):
                for i in tqdm(range(GROUPSIZE), desc="Group", total=GROUPSIZE, leave=False):
                    # print(f"Generating subject {i}: {desc(kind, corr_percent, n_clusters)}")
                    eigs = Eigenvalues.from_time_series(
                        data=make_signals(kind, corr_percent, n_clusters, N, T, NOISE),
                        covariance=False,
                        trim_zeros=False,
                    )
                    # manually trim zero-valued eigenvalue
                    eigs_all[kind][corr_percent][n_clusters][i, :] = eigs.vals[1:]

    return eigs_all


def precompute_simulated() -> AllEigs:
    sim_prefix = f"{CORR_PERCENTS}-percent-corr_clust-{N_CLUSTERS}"
    ALL_OUT: Path = DATA_ROOT / "Simulated" / f"{sim_prefix}_noise-{NOISE}.pickle"
    eigs: Optional[AllEigs] = None
    if not ALL_OUT.resolve().exists():
        print("No existing simulated groups found. Computing...")
        eigs = simulate_groups()
        with open(ALL_OUT.resolve(), "wb") as out:
            pickle.dump(eigs, out)
        print(f"Saved all eigenvalues to {ALL_OUT.relative_to(DATA_ROOT)}")
        if eigs is None:
            raise RuntimeError("`eigs` is none, this is likely a coding error.")
        return eigs
    with open(ALL_OUT.resolve(), "rb") as input_file:
        eigs = pickle.load(input_file)
        if eigs is None:
            raise RuntimeError("`eigs` is none, this is likely a coding error.")
        return eigs


def pre_largest(eigs: ndarray) -> DataFrame:
    """Take eigs, compute the largest, and return a DataFrame of the largest values"""
    n = eigs.shape[0]
    largest = DataFrame(
        index=["largest"],
        columns=["eigs-{:02d}" for i in range(n)],
        data=np.max(eigs, axis=1).reshape(1, n),
        dtype=float,
    )
    return largest


def pre_marchenko(eigs: ndarray) -> DataFrame:
    """Take eigs, compute the Marchenko-Pastur endpoints (both shifted and
    unshifted), and return a DataFrame of those values. DataFrame will also
    contain information rated to proportion of eigenvalues within those bounds
    "noise_ratio" and "noise_ratio" shifted."""
    df = DataFrame(
        index=["low", "high", "low_shift", "high_shift", "noise_ratio", "noise_ratio_shifted"],
        dtype=int,
    )
    desc = "eigs-{:02d} - Marchenko"
    groupsize = eigs.shape[0]
    pbar = tqdm(total=groupsize, desc=desc.format(0))
    for i in range(groupsize):
        vals = eigs[i, :]
        eig = Eigenvalues(vals)
        _, march = eig.trim_marchenko_pastur(series_length=T, n_series=N, use_shifted=False)
        _, march_shift = eig.trim_marchenko_pastur(series_length=T, n_series=N, use_shifted=True)
        noise_ratio = np.mean((vals > march[0]) & (vals < march[1]))
        noise_ratio_shifted = np.mean((vals > march_shift[0]) & (vals < march_shift[1]))
        df["eigs-{:02d}".format(i)] = [
            march[0],
            march[1],
            march_shift[0],
            march_shift[1],
            noise_ratio,
            noise_ratio_shifted,
        ]
        pbar.set_description(desc=desc.format(i))
        pbar.update()
    pbar.close()
    return df


def pre_brody(eigs: ndarray, args: Any) -> DataFrame:
    """Take eigs, compute the Brody parameter beta, and return a DataFrame in `out`"""
    if args.trim in ["(1,:)", "", "(0,:)"]:
        pass  # simulation already trims the spurious zero
    else:
        low, high = eval(args.trim)
        eigs = eigs[:, low:high]

    brod_df = DataFrame(index=["beta"], dtype=float)
    desc = "eigs-{:02d} - Brody"
    groupsize = eigs.shape[0]
    pbar = tqdm(total=groupsize, desc=desc.format(0))
    for i in range(groupsize):
        vals = eigs[i, :]
        unfolded = Eigenvalues(vals).unfold(**args.unfold)
        # print(f"\t\tComputing Brody fit for {str(path.resolve().name)}...")
        pbar.set_description(desc=desc.format(i))
        pbar.update()
        brody = unfolded.fit_brody(**args.brody)
        brod_df["eigs-{:02d}".format(i)] = brody["beta"]
    pbar.close()
    return brod_df


def pre_nnsd(eigs: ndarray, args: Any, n_bins: int = 20) -> Tuple[DataFrame, DataFrame]:
    """Take eigs, compute the Brody parameter beta, and return two DataFrames, one of spacings,
    and one of kde

    Returns
    -------
    spacings: DataFrame
    kde_vals: DataFrame
    """
    if args.trim in ["(1,:)", "", "(0,:)"]:
        pass  # simulation already trims the spurious zero
    else:
        low, high = eval(args.trim)
        eigs = eigs[:, low:high]

    df_spacings = DataFrame(dtype=float)
    df_kde = DataFrame(dtype=float)
    desc = "eigs-{:02d} - NNSD"
    groupsize = eigs.shape[0]
    kde_grid = np.linspace(0, 3, 1000)
    pbar = tqdm(total=groupsize, desc=desc.format(0))
    for i in range(groupsize):
        spacings = Eigenvalues(eigs[i, :]).unfold(**args.unfold).spacings
        kde = _kde(spacings, kde_grid)
        colname = "eigs-{:02d}".format(i)
        df_spacings[colname] = spacings
        df_kde[colname] = kde
        pbar.set_description(desc=desc.format(i))
        pbar.update()
    pbar.close()
    return df_spacings, df_kde


def pre_rigidity(eigs: ndarray, args: Any) -> DataFrame:
    """Take eigs, compute the rigidity, and return a DataFrame in `out`"""
    if args.trim in ["(1,:)", "", "(0,:)"]:
        pass  # simulation already trims the spurious zero
    else:
        low, high = eval(args.trim)
        eigs = eigs[:, low:high]

    df = DataFrame(dtype=float)
    desc = "eigs-{:02d} - Rigidity"
    groupsize = eigs.shape[0]
    pbar = tqdm(total=groupsize, desc=desc.format(0))
    for i in range(groupsize):
        unfolded = Eigenvalues(eigs[i, :]).unfold(**args.unfold)
        pbar.set_description(desc=desc.format(i))
        rigidity = unfolded.spectral_rigidity(**args.rigidity)
        pbar.update()
        if df.get("L") is None:
            df["L"] = rigidity["L"]
        df["eigs-{:02d}".format(i)] = rigidity["delta"]
    pbar.close()
    df.set_index("L", inplace=True)
    return df


def pre_levelvar(eigs: ndarray, args: Any) -> DataFrame:
    """Take eigs, compute the levelvar, and return a DataFrame
    in `out`"""
    if args.trim in ["(1,:)", "", "(0,:)"]:
        pass  # simulation already trims the spurious zero
    else:
        low, high = eval(args.trim)
        eigs = eigs[:, low:high]

    df = DataFrame(dtype=float)
    desc = "eigs-{:02d} - Levelvar"
    groupsize = eigs.shape[0]
    pbar = tqdm(total=groupsize, desc=desc.format(0))
    for i in range(groupsize):
        unfolded = Eigenvalues(eigs[i, :]).unfold(**args.unfold)
        pbar.set_description(desc=desc.format(i))
        levelvar = unfolded.level_variance(**args.levelvar)
        pbar.update()
        if df.get("L") is None:
            df["L"] = levelvar["L"]
        df["eigs-{:02d}".format(i)] = levelvar["sigma"]
    pbar.close()
    df.set_index("L", inplace=True)
    return df


def precompute_observables(
    args: Any,
    force_all: bool = False,
    force_largest: bool = False,
    force_marchenko: bool = False,
    force_brody: bool = False,
    force_nnsd: bool = False,
    force_rigidity: bool = False,
    force_levelvar: bool = False,
    disable_progress: bool = False,
) -> SummaryDict:
    def rel(path: Path) -> str:
        return str(path.resolve().relative_to(DATA_ROOT))

    if force_all:
        force_largest = (
            force_marchenko
        ) = force_brody = force_nnsd = force_rigidity = force_levelvar = True

    eigs_all = precompute_simulated()
    log = Log()
    print_log = False  # will be changed if any calculated
    summaries: SummaryDict = {}
    for kind in tqdm(["norm", "unif"], desc="Kind", total=2, disable=disable_progress):
        summaries[kind] = {}
        corr_percents = list(eigs_all[kind].keys())
        for corr in tqdm(
            corr_percents, desc="Corr", total=len(corr_percents), disable=disable_progress
        ):
            summaries[kind][corr] = {}
            cluster_sizes = list(eigs_all[kind][corr].keys())
            for clust_size in tqdm(
                cluster_sizes, desc="Clust", total=len(cluster_sizes), disable=disable_progress
            ):
                summaries[kind][corr][clust_size] = {}
                outs = summaryfiles_from_args(args, kind, corr, clust_size)
                # fmt: off
                largest_out   = outs["largest"]
                marchenko_out = outs["marchenko"]
                brody_out     = outs["brody"]
                nnsd_out      = outs["nnsd"]
                rigidity_out  = outs["rigidity"]
                levelvar_out  = outs["levelvar"]
                # fmt: on

                eigs = eigs_all[kind][corr][clust_size]

                if force_largest or not largest_out.exists():
                    print_log = True
                    largest = pre_largest(eigs)
                    pd.to_pickle(largest, largest_out)
                    log.append(f"Saved largest eigenvalues to {rel(largest_out)}")
                else:
                    log.append(f"Largest eigenvalues already exist at {rel(largest_out)}")
                summaries[kind][corr][clust_size]["largest"] = largest_out

                if force_marchenko or not marchenko_out.exists():
                    print_log = True
                    marchenko = pre_marchenko(eigs)
                    pd.to_pickle(marchenko, marchenko_out)
                    log.append(f"Saved Marchenko data to {rel(marchenko_out)}")
                else:
                    log.append(f"Marchenko data already exists at {rel(marchenko_out)}")
                summaries[kind][corr][clust_size]["marchenko"] = marchenko_out

                if force_brody or not brody_out.exists():
                    print_log = True
                    brody = pre_brody(eigs, args)
                    pd.to_pickle(brody, brody_out)
                    log.append(f"Saved Brody data to {rel(brody_out)}")
                else:
                    log.append(f"Brody data already exists at {rel(brody_out)}")
                summaries[kind][corr][clust_size]["brody"] = brody_out

                if force_nnsd or not nnsd_out.exists():
                    print_log = True
                    nnsd = pre_nnsd(eigs, args, n_bins=20)
                    pd.to_pickle(nnsd, nnsd_out)
                    log.append(f"Saved NNSD data to {rel(nnsd_out)}")
                else:
                    log.append(f"NNSD data already exists at {rel(nnsd_out)}")
                summaries[kind][corr][clust_size]["nnsd"] = nnsd_out

                if force_rigidity or not rigidity_out.exists():
                    print_log = True
                    rigidity = pre_rigidity(eigs, args)
                    pd.to_pickle(rigidity, rigidity_out)
                    log.append(f"Saved rigidity data to {rel(rigidity_out)}")
                else:
                    log.append(f"Rigidity data already exists at {rel(rigidity_out)}")
                summaries[kind][corr][clust_size]["rigidity"] = rigidity_out

                if force_levelvar or not levelvar_out.exists():
                    print_log = True
                    levelvar = pre_levelvar(eigs, args)
                    pd.to_pickle(levelvar, levelvar_out)
                    log.append(f"Saved levelvar data to {rel(levelvar_out)}")
                else:
                    log.append(f"Levelvar data already exists at {rel(levelvar_out)}")
                summaries[kind][corr][clust_size]["levelvar"] = levelvar_out
                if not disable_progress and print_log:
                    print(f"Computed {kind}_corr-{corr}%_clustsize-{clust_size}.")
    if print_log:
        log.print()
    return summaries


def plot_largest(summary: Path, title: str, mode: PlotMode = "show", ax: plt.Axes = None) -> None:
    c1 = "#000000"
    largest: DataFrame = pd.read_pickle(summary)
    if ax is None:
        sbn.set_context("paper")
        sbn.set_style("ticks", {"ytick.left": False})
        fig, axis = plt.subplots()
    else:
        axis = ax
    sbn.distplot(
        largest, bins=10, hist=False, kde=True, rug=True, color=c1, norm_hist=True, ax=axis
    )
    axis.set_title(title)
    axis.text(0.1, 0.8, f"μ = {np.round(float(largest.mean(axis=1)), 0)}", transform=axis.transAxes)
    axis.text(0.1, 0.7, f"σ = {np.round(float(largest.std(axis=1)), 2)}", transform=axis.transAxes)
    if ax is not None:
        return

    if mode == "show":
        plt.show()
        plt.close()
        return

    if mode == "save":
        fig.set_size_inches(4, 4)
        outfile = PLOT_OUTDIR / f"{summary.stem}.png"
        fig.savefig(outfile, dpi=300)
        print(f"Saved largest eigenvalues plot to {outfile}")


def plot_largest_all(summaries: SummaryDict, show: bool = False) -> None:
    sbn.set_context("paper")
    sbn.set_style("ticks", {"ytick.left": False})
    nrows, ncols = 2 * len(CORR_PERCENTS), len(N_CLUSTERS)
    fig: plt.Figure
    fig, axes = plt.subplots(nrows, ncols, sharex=False, sharey=True)
    i = 0  # index into axes
    for kind in summaries:
        for corr in summaries[kind]:
            for clust_size in summaries[kind][corr]:
                ax = axes.flat[i]
                title = f"{corr}% corr, n_clust={clust_size} ({kind})"
                plot_largest(
                    summaries[kind][corr][clust_size]["largest"], title, mode="show", ax=ax
                )
                i += 1
    fig.suptitle(f"Largest Eigenvalues")
    fig.set_size_inches(w=3 * ncols + 2, h=2 * nrows)
    fig.subplots_adjust(left=0.13, bottom=0.07, top=0.94, wspace=0.5, hspace=0.35)
    fig.text(0.5, 0.04, "Eigenvalue Magnitude", ha="center", va="center")  # xlabel
    fig.text(0.05, 0.5, "Density", ha="center", va="center", rotation="vertical")  # ylabel
    if show:
        plt.show()
        plt.close()
        return
    outfile = PLOT_OUTDIR / "largest_all.png"
    fig.savefig(outfile.resolve(), dpi=300)
    print(f"Saved largest eigenvalues plot to {outfile.resolve()}.")


def plot_marchenko(summary: Path, title: str, mode: PlotMode = "show", ax: plt.Axes = None) -> None:
    c1 = "#000000"
    df = pd.read_pickle(summary)
    if ax is None:
        sbn.set_context("paper")
        sbn.set_style("ticks", {"ytick.left": False})
        fig, axis = plt.subplots()
    else:
        axis = ax
    noise = df.loc["noise_ratio"].to_numpy(dtype=np.float64).ravel()
    if np.allclose(noise, np.zeros_like(noise)):
        axis.text(0.45, 0.45, "N/A", transform=axis.transAxes)
    else:
        sbn.distplot(
            noise, bins=10, hist=False, kde=True, rug=True, color=c1, norm_hist=True, ax=axis
        )
        axis.text(0.1, 0.8, "μ = {:.2e}".format(np.mean(noise)), transform=axis.transAxes)
        axis.text(0.1, 0.7, "σ = {:.2e}".format(np.std(noise, ddof=1)), transform=axis.transAxes)
    axis.set_title(title)
    if ax is not None:
        return

    if mode == "show":
        plt.show()
        plt.close()
        return

    if mode == "save":
        fig.set_size_inches(4, 4)
        outfile = PLOT_OUTDIR / f"{summary.stem}.png"
        fig.savefig(outfile, dpi=300)
        print(f"Saved Marchenko noise ratio plot to {outfile}")


def plot_marchenko_all(summaries: SummaryDict, show: bool = False) -> None:
    sbn.set_context("paper")
    sbn.set_style("ticks", {"ytick.left": False})
    nrows, ncols = 2 * len(CORR_PERCENTS), len(N_CLUSTERS)
    fig: plt.Figure
    fig, axes = plt.subplots(nrows, ncols, sharex=False, sharey=False)
    i = 0  # index into axes
    for kind in summaries:
        for corr in summaries[kind]:
            for clust_size in summaries[kind][corr]:
                ax = axes.flat[i]
                title = f"{corr}% corr, n_clust={clust_size} ({kind})"
                plot_marchenko(
                    summaries[kind][corr][clust_size]["marchenko"], title, mode="show", ax=ax
                )
                i += 1
    fig.suptitle(f"Marchenko noise")
    fig.set_size_inches(w=3 * ncols + 2, h=2 * nrows)
    fig.subplots_adjust(left=0.13, bottom=0.07, top=0.94, wspace=0.5, hspace=0.35)
    fig.text(0.5, 0.04, "Noise Ratio", ha="center", va="center")  # xlabel
    fig.text(0.05, 0.5, "Density", ha="center", va="center", rotation="vertical")  # ylabel
    if show:
        plt.show()
        plt.close()
        return
    outfile = PLOT_OUTDIR / "marchenko_all.png"
    fig.savefig(outfile, dpi=300)
    print(f"Saved Marchenko noise plot to {outfile.resolve()}.")


def plot_marchenko_shifted(
    summary: Path, title: str, mode: PlotMode = "show", ax: plt.Axes = None
) -> None:
    c1 = "#000000"
    df = pd.read_pickle(summary)
    if ax is None:
        sbn.set_context("paper")
        sbn.set_style("ticks", {"ytick.left": False})
        fig, axis = plt.subplots()
    else:
        axis = ax
    noise = df.loc["noise_ratio_shifted"].to_numpy(dtype=np.float64).ravel()
    if np.allclose(noise, np.zeros_like(noise)):
        axis.text(0.45, 0.45, "N/A", transform=axis.transAxes)
    else:
        sbn.distplot(
            noise,
            bins=10,
            hist=False,
            kde=True,
            rug=True,
            color=c1,
            norm_hist=True,
            axlabel="Noise Ratio",
            ax=axis,
        )
        axis.text(0.1, 0.8, "μ = {:.2e}".format(np.mean(noise)), transform=axis.transAxes)
        axis.text(0.1, 0.7, "σ = {:.2e}".format(np.std(noise, ddof=1)), transform=axis.transAxes)
    axis.set_title(f"{title} (shifted)")
    axis.set_ylabel("")
    if ax is not None:
        return

    if mode == "show":
        plt.show()
        plt.close()
        return

    if mode == "save":
        fig.set_size_inches(4, 4)
        outfile = PLOT_OUTDIR / f"{summary.stem}_shifted.png"
        fig.savefig(outfile, dpi=300)
        print(f"Saved Marchenko shifted noise ratio plot to {outfile}")


def plot_marchenko_shifted_all(summaries: SummaryDict, show: bool = False) -> None:
    sbn.set_context("paper")
    sbn.set_style("ticks", {"ytick.left": False})
    nrows, ncols = 2 * len(CORR_PERCENTS), len(N_CLUSTERS)
    fig: plt.Figure
    fig, axes = plt.subplots(nrows, ncols, sharex=False, sharey=True)
    i = 0  # index into axes
    for kind in summaries:
        for corr in summaries[kind]:
            for clust_size in summaries[kind][corr]:
                ax = axes.flat[i]
                title = f"{corr}% corr, n_clust={clust_size} ({kind})"
                plot_marchenko_shifted(
                    summaries[kind][corr][clust_size]["marchenko"], title, mode="show", ax=ax
                )
                i += 1
    fig.suptitle(f"Marchenko noise (shifted)")
    fig.set_size_inches(w=3 * ncols + 2, h=2 * nrows)
    fig.subplots_adjust(left=0.13, bottom=0.07, top=0.94, wspace=0.5, hspace=0.35)
    fig.text(0.5, 0.04, "Noise Ratio", ha="center", va="center")  # xlabel
    fig.text(0.05, 0.5, "Density", ha="center", va="center", rotation="vertical")  # ylabel
    if show:
        plt.show()
        plt.close()
        return
    outfile = PLOT_OUTDIR / "marchenko_shifted_all.png"
    fig.savefig(outfile, dpi=300)
    print(f"Saved shifted Marchenko noise plot to {outfile.resolve()}.")


def plot_nnsd(
    nnsd_summary: Path,
    title: str,
    mode: PlotMode = "show",
    ax: plt.Axes = None,
    n_boot: int = 1000,
    brody: bool = True,
) -> None:
    c1, c2, c3 = "#000000", "#9d0000", "#FD8208"
    spacings, kde = pd.read_pickle(nnsd_summary)
    axis: plt.Axes
    if ax is None:
        sbn.set_context("paper")
        sbn.set_style("ticks", {"ytick.left": False})
        fig, axis = plt.subplots()
    else:
        axis = ax
    bins = np.linspace(0, 3, 11)  # 10 bins
    alpha = 1.0 / len(spacings.columns)
    for colname in spacings:
        s = spacings[colname].to_numpy(dtype=float).ravel()
        s = s[(s >= 0) & (s <= 3)]
        sbn.distplot(
            s, bins=bins, kde=False, color=c1, norm_hist=True, ax=axis, hist_kws={"alpha": alpha}
        )

    grid = np.linspace(0, 3, 1000)  # forgot to save this in df
    boots = _percentile_boot(kde, B=n_boot)
    sbn.lineplot(x=grid, y=boots["mean"], color=c2, label="Mean KDE", ax=axis)
    axis.fill_between(x=grid, y1=boots["low"], y2=boots["high"], color=c2, alpha=0.3)
    # sbn.lineplot(x=s, y=kde.mean(axis=0), color=c1, label="Mean KDE", ax=axis)
    sbn.lineplot(x=grid, y=Poisson.nnsd(spacings=grid), color="#08FD4F", label="Poisson", ax=axis)
    sbn.lineplot(x=grid, y=GOE.nnsd(spacings=grid), color="#0066FF", label="GOE", ax=axis)

    # need to do Brody last to ensure visible
    if brody:
        betas = []
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            for i, colname in enumerate(spacings):
                s = spacings[colname].to_numpy(dtype=float).ravel()
                s = s[(s >= 0) & (s <= 3)]
                alpha = max((1.0 / len(spacings.columns), 0.3))
                beta = fit_brody(s, method="mle")
                betas.append(beta)
                if i == 0:  # only add a label once (legend hack)
                    sbn.lineplot(
                        x=s,
                        y=brody_dist(s, beta),
                        color=c3,
                        alpha=alpha,
                        ax=axis,
                        label="Brody fit",
                    )
                else:
                    sbn.lineplot(x=s, y=brody_dist(s, beta), color=c3, alpha=alpha, ax=axis)

        axis.text(0.6, 0.8, f"<β> = {np.round(float(np.mean(betas)), 2)}", transform=axis.transAxes)

    axis.set_ylabel("")
    axis.set_title(title)
    handle, labels = axis.get_legend_handles_labels()
    axis._remove_legend(handle)
    if ax is not None:
        return

    if mode == "show":
        plt.show()
        plt.close()
        return

    if mode == "save":
        fig.set_size_inches(4, 4)
        outfile = PLOT_OUTDIR / f"{nnsd_summary.stem}.png"
        fig.savefig(outfile, dpi=300)
        print(f"Saved NNSD plot to {outfile}")


def plot_nnsd_all(
    summaries: SummaryDict, n_boot: int = 5000, brody: bool = True, show: bool = False
) -> None:
    sbn.set_context("paper")
    sbn.set_style("ticks", {"ytick.left": False})
    nrows, ncols = 2 * len(CORR_PERCENTS), len(N_CLUSTERS)
    fig: plt.Figure
    axes: plt.Axes
    fig, axes = plt.subplots(nrows, ncols, sharex=False, sharey=True)
    pbar = tqdm(desc="Plotting NNSD...", total=nrows * ncols - 1)
    i = 0  # index into axes
    for kind in sorted(summaries.keys()):
        for corr in sorted(summaries[kind].keys()):
            for clust_size in sorted(summaries[kind][corr].keys()):
                ax = axes.flat[i]
                title = f"{corr}% corr, n_clust={clust_size} ({kind})"
                plot_nnsd(
                    summaries[kind][corr][clust_size]["nnsd"],
                    title,
                    brody=brody,
                    mode="show",
                    ax=ax,
                    n_boot=n_boot,
                )
                pbar.update()
                i += 1
    pbar.close()
    fig.suptitle(f"NNSD")
    plt.setp(axes, ylim=(0.0, 1.2))  # better use of space
    fig.set_size_inches(w=3 * ncols + 2, h=2 * nrows)
    fig.subplots_adjust(left=0.13, bottom=0.07, top=0.94, wspace=0.5, hspace=0.35)
    fig.text(0.5, 0.04, "spacings (s)", ha="center", va="center")  # xlabel
    fig.text(0.05, 0.5, "density (p(s))", ha="center", va="center", rotation="vertical")  # ylabel
    handles, labels = axes.flat[-1].get_legend_handles_labels()
    fig.legend(handles, labels, loc="center right", frameon=False, framealpha=0, fontsize="8")
    if show:
        plt.show()
        plt.close()
        return
    outfile = PLOT_OUTDIR / "nnsd_all.png"
    fig.savefig(outfile, dpi=300)
    print(f"Saved NNSD plot to {outfile.resolve()}")


def plot_rigidity(
    summary: Path, title: str, mode: PlotMode = "show", ax: plt.Axes = None, n_boot: int = 5000
) -> None:
    c1, c2 = "#000000", "#9d0000"
    df: DataFrame = pd.read_pickle(summary)
    axis: plt.Axes
    if ax is None:
        sbn.set_context("paper")
        sbn.set_style("ticks", {"ytick.left": False})
        fig, axis = plt.subplots()
    else:
        axis = ax

    L = df.index.to_numpy(dtype=np.float64).ravel()
    for colname in df:
        rigidity = df[colname].to_numpy(dtype=float).ravel()
        sbn.lineplot(x=L, y=rigidity, color=c1, legend=False, alpha=0.1, ax=axis)

    grid = np.linspace(L.min(), L.max(), 1000)
    boots = _percentile_boot(df, B=n_boot)
    sbn.lineplot(x=L, y=boots["mean"], color=c2, label="Mean", ax=axis)
    axis.fill_between(x=L, y1=boots["low"], y2=boots["high"], color=c2, alpha=0.3)
    # sbn.lineplot(x=s, y=kde.mean(axis=0), color=c1, label="Mean KDE", ax=axis)
    sbn.lineplot(
        x=grid, y=Poisson.spectral_rigidity(L=grid), color="#08FD4F", label="Poisson", ax=axis
    )
    sbn.lineplot(x=grid, y=GOE.spectral_rigidity(L=grid), color="#0066FF", label="GOE", ax=axis)

    axis.set_title(title)
    axis.set_ylabel("")
    handle, labels = axis.get_legend_handles_labels()
    axis._remove_legend(handle)
    if ax is not None:
        return

    if mode == "show":
        plt.show()
        plt.close()
        return

    if mode == "save":
        fig.set_size_inches(4, 4)
        outfile = PLOT_OUTDIR / f"{summary.stem}.png"
        fig.savefig(outfile, dpi=300)
        print(f"Saved NNSD plot to {outfile}")


def plot_rigidity_all(summaries: SummaryDict, n_boot: int = 5000, show: bool = False) -> None:
    sbn.set_context("paper")
    sbn.set_style("ticks", {"ytick.left": False})
    nrows, ncols = 2 * len(CORR_PERCENTS), len(N_CLUSTERS)
    fig: plt.Figure
    axes: plt.Axes
    fig, axes = plt.subplots(nrows, ncols, sharex=False, sharey=False)
    pbar = tqdm(desc="Plotting Rigidity...", total=nrows * ncols - 1)
    i = 0  # index into axes
    for kind in summaries:
        for corr in summaries[kind]:
            for clust_size in summaries[kind][corr]:
                ax = axes.flat[i]
                title = f"{corr}% corr, n_clust={clust_size} ({kind})"
                plot_rigidity(
                    summaries[kind][corr][clust_size]["rigidity"],
                    title,
                    mode="show",
                    ax=ax,
                    n_boot=n_boot,
                )
                pbar.update()
                i += 1
    pbar.close()
    fig.suptitle(f"Spectral Rigidity")
    plt.setp(axes, ylim=(0.0, 0.75))  # better use of space
    fig.set_size_inches(w=3 * ncols + 2, h=2 * nrows)
    fig.subplots_adjust(left=0.13, bottom=0.07, top=0.94, wspace=0.5, hspace=0.35)
    fig.text(0.5, 0.04, "L", ha="center", va="center")  # xlabel
    fig.text(
        0.05, 0.5, "∆₃(L)", ha="center", va="center", rotation="vertical", fontname="DejaVu Sans"
    )  # ylabel
    handles, labels = axes.flat[-1].get_legend_handles_labels()
    fig.legend(handles, labels, loc="center right", frameon=False, framealpha=0, fontsize="8")
    if show:
        plt.show()
        plt.close()
        return
    outfile = PLOT_OUTDIR / "rigidity_all.png"
    fig.savefig(outfile, dpi=300)
    print(f"Saved rigidity plot to {outfile.resolve()}.")


def plot_levelvar(
    summary: Path, title: str, mode: PlotMode = "show", ax: plt.Axes = None, n_boot: int = 5000
) -> None:
    c1, c2 = "#000000", "#9d0000"
    df: DataFrame = pd.read_pickle(summary)
    axis: plt.Axes
    if ax is None:
        sbn.set_context("paper")
        sbn.set_style("ticks", {"ytick.left": False})
        fig, axis = plt.subplots()
    else:
        axis = ax

    L = df.index.to_numpy(dtype=np.float64).ravel()
    for colname in df:
        levelvar = df[colname].to_numpy(dtype=float).ravel()
        sbn.lineplot(x=L, y=levelvar, color=c1, legend=False, alpha=0.1, ax=axis)

    grid = np.linspace(L.min(), L.max(), 1000)
    boots = _percentile_boot(df, B=n_boot)
    sbn.lineplot(x=L, y=boots["mean"], color=c2, label="Mean", ax=axis)
    axis.fill_between(x=L, y1=boots["low"], y2=boots["high"], color=c2, alpha=0.3)
    # sbn.lineplot(x=s, y=kde.mean(axis=0), color=c1, label="Mean KDE", ax=axis)
    sbn.lineplot(
        x=grid, y=Poisson.level_variance(L=grid), color="#08FD4F", label="Poisson", ax=axis
    )
    sbn.lineplot(x=grid, y=GOE.level_variance(L=grid), color="#0066FF", label="GOE", ax=axis)

    axis.set_title(title)
    axis.set_ylabel("")
    handle, labels = axis.get_legend_handles_labels()
    axis._remove_legend(handle)
    if ax is not None:
        return

    if mode == "show":
        plt.show()
        plt.close()
        return

    if mode == "save":
        fig.set_size_inches(4, 4)
        outfile = PLOT_OUTDIR / f"{summary.stem}.png"
        fig.savefig(outfile, dpi=300)
        print(f"Saved levelvar plot to {outfile}")


def plot_levelvar_all(summaries: SummaryDict, n_boot: int = 5000, show: bool = False) -> None:
    sbn.set_context("paper")
    sbn.set_style("ticks", {"ytick.left": False})
    nrows, ncols = 2 * len(CORR_PERCENTS), len(N_CLUSTERS)
    fig: plt.Figure
    axes: plt.Axes
    fig, axes = plt.subplots(nrows, ncols, sharex=False, sharey=False)
    pbar = tqdm(desc="Plotting levelvar...", total=nrows * ncols - 1)
    i = 0  # index into axes
    for kind in sorted(summaries.keys()):
        for corr in sorted(summaries[kind].keys()):
            for clust_size in sorted(summaries[kind][corr].keys()):
                ax = axes.flat[i]
                title = f"{corr}% corr, n_clust={clust_size} ({kind})"
                plot_levelvar(
                    summaries[kind][corr][clust_size]["levelvar"],
                    title,
                    mode="show",
                    ax=ax,
                    n_boot=n_boot,
                )
                pbar.update()
                i += 1
    pbar.close()
    fig.suptitle(f"Level Number Variance")
    fig.set_size_inches(12, 14)
    # plt.setp(axes, ylim=(0.0, 0.75))  # better use of space
    fig.subplots_adjust(left=0.13, bottom=0.07, top=0.94, wspace=0.25, hspace=0.35)
    fig.text(0.5, 0.04, "L", ha="center", va="center")  # xlabel
    fig.text(
        0.05, 0.5, "Σ²(L)", ha="center", va="center", rotation="vertical", fontname="DejaVu Sans"
    )  # ylabel
    handles, labels = axes.flat[-1].get_legend_handles_labels()
    fig.legend(handles, labels, loc="center right", frameon=False, framealpha=0, fontsize="8")
    if show:
        plt.show()
        plt.close()
        return
    outfile = PLOT_OUTDIR / "levelvar_all.png"
    fig.savefig(outfile, dpi=300)
    print(f"Saved levelvar plot to {outfile.resolve()}.")


summaries = precompute_observables(ARGS, force_all=False, disable_progress=True)
# plot_largest_all(summaries, show=False)
# plot_marchenko_all(summaries, show=False)
plot_nnsd_all(summaries, n_boot=50, brody=True, show=False)
# plot_rigidity_all(summaries, n_boot=500, show=False)
# plot_levelvar_all(summaries, n_boot=500, show=False)

# raise

# for kind in summaries:
#     nrows, ncols = len(CORR_PERCENTS), len(N_CLUSTERS)
#     fig, axes = plt.subplots(nrows, ncols, sharex=True, sharey=True)
#     for corr in summaries[kind]:
#         for clust_size in summaries[kind][corr]:
#             title = f"{corr}% correlated, clusters of size {clust_size} ({kind})"

#             # title_marchenko = f"{title} - Marchenko"
#             # plot_marchenko(
#             #     summaries[kind][corr][clust_size]["marchenko"], title_marchenko, mode="show"
#             # )
#             # plot_marchenko_shifted(
#             #     summaries[kind][corr][clust_size]["marchenko"], title_marchenko, mode="show"
#             # )

#             # title_nnsd = f"{title} - NNSD"
#             # plot_nnsd(summaries[kind][corr][clust_size]["nnsd"], title_nnsd, mode="show")

#             title_rigidity = f"{title} - Rigidity"
#             plot_rigidity(
#                 summaries[kind][corr][clust_size]["rigidity"],
#                 title_rigidity,
#                 mode="show",
#                 n_boot=1000,
#             )
#             raise
