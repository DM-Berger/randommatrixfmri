python3 ../../extract.py derivatives/task/sub-01_task-imagery_run-03_bold.nii.gz derivatives/task/sub-01_task-imagery_run-03_bold_mask.nii.gz rmt/task/eigs-03.npy &
python3 ../../extract.py derivatives/task/sub-01_task-imagery_run-02_bold.nii.gz derivatives/task/sub-01_task-imagery_run-02_bold_mask.nii.gz rmt/task/eigs-02.npy &
python3 ../../extract.py derivatives/task/sub-01_task-imagery_run-01_bold.nii.gz derivatives/task/sub-01_task-imagery_run-01_bold_mask.nii.gz rmt/task/eigs-01.npy &
python3 ../../extract.py derivatives/task/sub-01_task-imagery_run-00_bold.nii.gz derivatives/task/sub-01_task-imagery_run-00_bold_mask.nii.gz rmt/task/eigs-00.npy &
python3 ../../extract.py derivatives/rest/sub-01_task-rest_bold.nii.gz derivatives/rest/sub-01_task-rest_bold_mask.nii.gz rmt/rest/eigs-01.npy
