import matplotlib.pyplot as plt
import numba
import numpy as np
import os
import pandas as pd
import pickle
import seaborn as sbn
import warnings

from empyricalRMT.brody import brody_dist, fit_brody
from empyricalRMT.eigenvalues import Eigenvalues
from empyricalRMT.ensemble import GOE, Poisson
from numpy import ndarray
from pandas import DataFrame
from pathlib import Path
from time import ctime
from tqdm import tqdm

from _precompute import argstrings_from_args
from _utilities import _kde, _percentile_boot

from typing import Dict, List, Optional, Tuple
from typing_extensions import Literal

# fmt: off
TRIM_ARGS     = r"(0,-1)"  # must be indices of trims, tuple, no spaces
UNFOLD_ARGS   = {"smoother": "poly", "degree": 7, "detrend": False}
LEVELVAR_ARGS = {"L": np.arange(0.5, 10, 0.1), "tol": 0.001, "max_L_iters": 50000}
RIGIDITY_ARGS = {"L": np.arange(2, 20, 0.5)}
BRODY_ARGS    = {"method": "mle"}
# fmt: on

N = 200_000
T = 300
GROUPSIZE = 20
NOISE = 0.5


def make_signals(
    kind: str, corr_percent: int, N: int = N, t: int = T, noise: float = NOISE
) -> ndarray:
    base = None
    if kind == "norm" or kind == "normal":
        base = np.random.standard_normal(t)
    elif kind == "unif" or kind == "uniform":
        base = np.random.uniform(0, 1, [t])
    else:
        raise ValueError("Invalid kind.")

    signals = np.empty([N, t])
    n_correlated = np.round(corr_percent / 100 * N)
    for i in range(n_correlated):
        signals[(i) * clust_size : (i + 1) * clust_size, :] = bases[i, :] + np.random.normal(
            0, noise, [clust_size, t]
        )
    if kind == "norm" or kind == "normal":
        signals[-n_iid:, :] = np.random.standard_normal([n_iid, t])
    if kind == "unif" or kind == "uniform":
        signals[-n_iid:, :] = np.random.uniform(0, 1, [n_iid, t])
    return signals

@jit(nopython=True, parallel=True, fastmath=True, cache=True)
def _make_correlated(n_correlated: int, N: int, t: int) -> ndarray:
