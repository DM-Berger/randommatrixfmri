import matplotlib.pyplot as plt
import numpy as np
import seaborn as sbn

from empyricalRMT.construct import generate_eigs
from empyricalRMT.eigenvalues import Eigenvalues
from empyricalRMT.plot import _spacings, _spectral_rigidity, _level_number_variance
from pathlib import Path


def setup_plotting():
    sbn.set_context("paper", font_scale=0.5)
    sbn.set_style("white")
    PALETTE = sbn.color_palette("dark").copy()
    PALETTE.insert(0, (0.0, 0.0, 0.0))
    sbn.set()
    sbn.set_palette(PALETTE)


def goe_plots():
    setup_plotting()
    unfolded = Eigenvalues(generate_eigs(10000)).unfold_goe()
    unf = unfolded.vals
    rig = unfolded.spectral_rigidity(L=np.arange(2, 50, 0.5), show_progress=True)
    var = unfolded.level_variance(L=np.arange(2, 30, 0.25), show_progress=True)

    fig: plt.Figure
    axes: plt.Axes
    fig, axes = plt.subplots(nrows=1, ncols=3)
    _spacings(
        unf,
        kde=False,
        title="NNSD",
        ensembles=["goe", "poisson"],
        mode="return",
        fig=fig,
        axes=axes.flat[0],
    )
    _spectral_rigidity(
        unf,
        rig,
        ensembles=["goe", "poisson"],
        mode="return",
        fig=fig,
        axes=axes.flat[1],
    )
    _level_number_variance(
        unf,
        var,
        title="Level Number Variance",
        ensembles=["goe", "poisson"],
        mode="return",
        fig=fig,
        axes=axes.flat[2],
    )
    for ax in axes.flat:
        ax.legend().set_visible(False)
    fig.subplots_adjust(wspace=0.32)
    fig.set_size_inches(w=6, h=2)
    plt.show()
    fig.savefig(
        Path(__file__).resolve().parent / "figures" / "unf_features.png", dpi=300
    )


goe_plots()
