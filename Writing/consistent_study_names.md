# Dataset 1 (Gorgolewski et al.) -- PSYCH

K. J. Gorgolewski et al., “A high resolution 7-Tesla resting-state fMRI
test-retest dataset with cognitive and physiological measures,” Sci Data, vol.
2, no. 1, p. 140054, Dec. 2015, doi: 10.1038/sdata.2014.54.

## Conditions

    * PANAS-X "trait attention": "trait_lowattend" vs. "trait_attend"
    * CCPT "task attention":     "task_attend"     vs. "task_lowattend"
    * Mini NYC "vigilance":      "high_vigilance"  vs. "low_vigilance"

# Dataset 2 (Tetreault et al.) -- OSTEO

P. Tétreault, A. Mansour, E. Vachon-Presseau, T. J. Schnitzer, A. V. Apkarian,
and M. N. Baliki, “Brain Connectivity Predicts Placebo Response across Chronic
Pain Clinical Trials,” PLOS Biology, vol. 14, no. 10, p. e1002570, Oct. 2016,
doi: 10.1371/journal.pbio.1002570.

## Conditions

    * "nopain" / "controls"
    * "pain" / "knee osteoarthritis" + "placebo"
    * "duloxetine" / "knee osteoarthritis" + "duloxetine"
    * "allpain" = "pain" + "duloxetine" = "all osteoarthritis subjects"


# Dataset 3 (DuPre et al.) -- REFLECT / ECHO

E. DuPre, W.-M. Luh, and R. N. Spreng, “Multi-echo fMRI replication sample of
autobiographical memory, prospection and theory of mind reasoning tasks,” Sci
Data, vol. 3, Dec. 2016, doi: 10.1038/sdata.2016.116.

## Conditions

    * "task"
    * "rest"

## Task

    View affective images, perform tasks variously related to "reflection",
    memory, prospection, ToM reasoning. I.e. high-level but general cognition.

## Analytic Conditions

    * "summed" (add up echoes)
    * "interleaved" (create interleaved sequence)


# Dataset 4 (Madhyastha et al.) -- PARK

T. M. Madhyastha, M. K. Askren, P. Boord, and T. J. Grabowski, “Dynamic
Connectivity at Rest Predicts Attention Task Performance,” Brain Connectivity,
vol. 5, no. 1, pp. 45–59, Feb. 2015, doi: 10.1089/brain.2014.0248.

## Conditions

    * "park" (non-demented Parkinson's disease)
    * "control" (healthy matched controls)

## Task

    Attention Network Task

## Analytic Conditions

    * "full-pre" (fully preprocessed) --> rename to "full preproc"
    * "control_pre vs park_pre in figures"



# Dataset 5 (Schapiro et al.) -- LEARN

A. C. Schapiro, E. A. McDevitt, T. T. Rogers, S. C. Mednick, and K. A. Norman,
“Human hippocampal replay during rest prioritizes weakly learned information
and predicts memory performance,” Nat Commun, vol. 9, Sep. 2018, doi:
10.1038/s41467-018-06213-1.

## Conditions

    * "task" (memorizing or matching satellite images)
    * "rest" 


## Analytic Conditions

    * "full-pre" (fully preprocessed) --> rename to "full preproc"
    * "control_pre vs park_pre in figures"



# Code Changes needed

* rename chart "ECHO" labels to "REFLECT" (or vice-versa)
* rename stacked histogram PARKINSONS -> PARK (and subgroups to \_preproc)
* as an alternative to the stacked histograms, just overlay feature histograms
  with transparencies?
