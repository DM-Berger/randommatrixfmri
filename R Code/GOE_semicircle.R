library(ggplot2)

# e.g. an n x n matrix has dimension n
plotEigenSizes = function(dimension) {
  M = matrix(rnorm(dimension*dimension, 0, 1), dimension, dimension);
  M = (M + t(M)) / 2;
  Eigenvalues = as.data.frame(eigen(M, T, T)$values) # symmetric, so no complex entries
  p = ggplot(Eigenvalues) +
    geom_histogram(aes(x = Eigenvalues[,1], y = ..density..), bins = 100) +
    geom_density(aes(x = Eigenvalues[,1])) +
    labs(title = "Eigenvalues from GOE") + xlab("Eigenvalue") + ylab("Density");
  print(p)
};

plotEigenSizes(1000);
